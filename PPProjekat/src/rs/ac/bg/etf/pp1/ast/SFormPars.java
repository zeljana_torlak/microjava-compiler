// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class SFormPars extends FormPars {

    private Type Type;
    private String formParmName;
    private SqrBraceOpt SqrBraceOpt;
    private FormParsExtList FormParsExtList;

    public SFormPars (Type Type, String formParmName, SqrBraceOpt SqrBraceOpt, FormParsExtList FormParsExtList) {
        this.Type=Type;
        if(Type!=null) Type.setParent(this);
        this.formParmName=formParmName;
        this.SqrBraceOpt=SqrBraceOpt;
        if(SqrBraceOpt!=null) SqrBraceOpt.setParent(this);
        this.FormParsExtList=FormParsExtList;
        if(FormParsExtList!=null) FormParsExtList.setParent(this);
    }

    public Type getType() {
        return Type;
    }

    public void setType(Type Type) {
        this.Type=Type;
    }

    public String getFormParmName() {
        return formParmName;
    }

    public void setFormParmName(String formParmName) {
        this.formParmName=formParmName;
    }

    public SqrBraceOpt getSqrBraceOpt() {
        return SqrBraceOpt;
    }

    public void setSqrBraceOpt(SqrBraceOpt SqrBraceOpt) {
        this.SqrBraceOpt=SqrBraceOpt;
    }

    public FormParsExtList getFormParsExtList() {
        return FormParsExtList;
    }

    public void setFormParsExtList(FormParsExtList FormParsExtList) {
        this.FormParsExtList=FormParsExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Type!=null) Type.accept(visitor);
        if(SqrBraceOpt!=null) SqrBraceOpt.accept(visitor);
        if(FormParsExtList!=null) FormParsExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Type!=null) Type.traverseTopDown(visitor);
        if(SqrBraceOpt!=null) SqrBraceOpt.traverseTopDown(visitor);
        if(FormParsExtList!=null) FormParsExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Type!=null) Type.traverseBottomUp(visitor);
        if(SqrBraceOpt!=null) SqrBraceOpt.traverseBottomUp(visitor);
        if(FormParsExtList!=null) FormParsExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("SFormPars(\n");

        if(Type!=null)
            buffer.append(Type.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+formParmName);
        buffer.append("\n");

        if(SqrBraceOpt!=null)
            buffer.append(SqrBraceOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FormParsExtList!=null)
            buffer.append(FormParsExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [SFormPars]");
        return buffer.toString();
    }
}
