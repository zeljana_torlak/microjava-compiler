// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ExprExtListS extends ExprExtList {

    private Addop Addop;
    private Term Term;
    private ExprEnd ExprEnd;
    private ExprExtList ExprExtList;

    public ExprExtListS (Addop Addop, Term Term, ExprEnd ExprEnd, ExprExtList ExprExtList) {
        this.Addop=Addop;
        if(Addop!=null) Addop.setParent(this);
        this.Term=Term;
        if(Term!=null) Term.setParent(this);
        this.ExprEnd=ExprEnd;
        if(ExprEnd!=null) ExprEnd.setParent(this);
        this.ExprExtList=ExprExtList;
        if(ExprExtList!=null) ExprExtList.setParent(this);
    }

    public Addop getAddop() {
        return Addop;
    }

    public void setAddop(Addop Addop) {
        this.Addop=Addop;
    }

    public Term getTerm() {
        return Term;
    }

    public void setTerm(Term Term) {
        this.Term=Term;
    }

    public ExprEnd getExprEnd() {
        return ExprEnd;
    }

    public void setExprEnd(ExprEnd ExprEnd) {
        this.ExprEnd=ExprEnd;
    }

    public ExprExtList getExprExtList() {
        return ExprExtList;
    }

    public void setExprExtList(ExprExtList ExprExtList) {
        this.ExprExtList=ExprExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Addop!=null) Addop.accept(visitor);
        if(Term!=null) Term.accept(visitor);
        if(ExprEnd!=null) ExprEnd.accept(visitor);
        if(ExprExtList!=null) ExprExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Addop!=null) Addop.traverseTopDown(visitor);
        if(Term!=null) Term.traverseTopDown(visitor);
        if(ExprEnd!=null) ExprEnd.traverseTopDown(visitor);
        if(ExprExtList!=null) ExprExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Addop!=null) Addop.traverseBottomUp(visitor);
        if(Term!=null) Term.traverseBottomUp(visitor);
        if(ExprEnd!=null) ExprEnd.traverseBottomUp(visitor);
        if(ExprExtList!=null) ExprExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ExprExtListS(\n");

        if(Addop!=null)
            buffer.append(Addop.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Term!=null)
            buffer.append(Term.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ExprEnd!=null)
            buffer.append(ExprEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ExprExtList!=null)
            buffer.append(ExprExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ExprExtListS]");
        return buffer.toString();
    }
}
