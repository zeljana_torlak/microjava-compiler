// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class PublicMethodDeclAccessListS extends MethodDeclAccessList {

    private MethodDecl MethodDecl;
    private MethodDeclAccessList MethodDeclAccessList;

    public PublicMethodDeclAccessListS (MethodDecl MethodDecl, MethodDeclAccessList MethodDeclAccessList) {
        this.MethodDecl=MethodDecl;
        if(MethodDecl!=null) MethodDecl.setParent(this);
        this.MethodDeclAccessList=MethodDeclAccessList;
        if(MethodDeclAccessList!=null) MethodDeclAccessList.setParent(this);
    }

    public MethodDecl getMethodDecl() {
        return MethodDecl;
    }

    public void setMethodDecl(MethodDecl MethodDecl) {
        this.MethodDecl=MethodDecl;
    }

    public MethodDeclAccessList getMethodDeclAccessList() {
        return MethodDeclAccessList;
    }

    public void setMethodDeclAccessList(MethodDeclAccessList MethodDeclAccessList) {
        this.MethodDeclAccessList=MethodDeclAccessList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodDecl!=null) MethodDecl.accept(visitor);
        if(MethodDeclAccessList!=null) MethodDeclAccessList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodDecl!=null) MethodDecl.traverseTopDown(visitor);
        if(MethodDeclAccessList!=null) MethodDeclAccessList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodDecl!=null) MethodDecl.traverseBottomUp(visitor);
        if(MethodDeclAccessList!=null) MethodDeclAccessList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PublicMethodDeclAccessListS(\n");

        if(MethodDecl!=null)
            buffer.append(MethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDeclAccessList!=null)
            buffer.append(MethodDeclAccessList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PublicMethodDeclAccessListS]");
        return buffer.toString();
    }
}
