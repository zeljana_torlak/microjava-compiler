// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class PrivateAbstractXMethodDeclList extends XMethodDeclAccessList {

    private AbstractMethodDecl AbstractMethodDecl;
    private XMethodDeclAccessList XMethodDeclAccessList;

    public PrivateAbstractXMethodDeclList (AbstractMethodDecl AbstractMethodDecl, XMethodDeclAccessList XMethodDeclAccessList) {
        this.AbstractMethodDecl=AbstractMethodDecl;
        if(AbstractMethodDecl!=null) AbstractMethodDecl.setParent(this);
        this.XMethodDeclAccessList=XMethodDeclAccessList;
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.setParent(this);
    }

    public AbstractMethodDecl getAbstractMethodDecl() {
        return AbstractMethodDecl;
    }

    public void setAbstractMethodDecl(AbstractMethodDecl AbstractMethodDecl) {
        this.AbstractMethodDecl=AbstractMethodDecl;
    }

    public XMethodDeclAccessList getXMethodDeclAccessList() {
        return XMethodDeclAccessList;
    }

    public void setXMethodDeclAccessList(XMethodDeclAccessList XMethodDeclAccessList) {
        this.XMethodDeclAccessList=XMethodDeclAccessList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractMethodDecl!=null) AbstractMethodDecl.accept(visitor);
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseTopDown(visitor);
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseBottomUp(visitor);
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PrivateAbstractXMethodDeclList(\n");

        if(AbstractMethodDecl!=null)
            buffer.append(AbstractMethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(XMethodDeclAccessList!=null)
            buffer.append(XMethodDeclAccessList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PrivateAbstractXMethodDeclList]");
        return buffer.toString();
    }
}
