// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ForeachStatement extends Statement {

    private ForeachName ForeachName;
    private String I2;
    private Designator Designator;
    private ForStart ForStart;
    private Statement Statement;

    public ForeachStatement (ForeachName ForeachName, String I2, Designator Designator, ForStart ForStart, Statement Statement) {
        this.ForeachName=ForeachName;
        if(ForeachName!=null) ForeachName.setParent(this);
        this.I2=I2;
        this.Designator=Designator;
        if(Designator!=null) Designator.setParent(this);
        this.ForStart=ForStart;
        if(ForStart!=null) ForStart.setParent(this);
        this.Statement=Statement;
        if(Statement!=null) Statement.setParent(this);
    }

    public ForeachName getForeachName() {
        return ForeachName;
    }

    public void setForeachName(ForeachName ForeachName) {
        this.ForeachName=ForeachName;
    }

    public String getI2() {
        return I2;
    }

    public void setI2(String I2) {
        this.I2=I2;
    }

    public Designator getDesignator() {
        return Designator;
    }

    public void setDesignator(Designator Designator) {
        this.Designator=Designator;
    }

    public ForStart getForStart() {
        return ForStart;
    }

    public void setForStart(ForStart ForStart) {
        this.ForStart=ForStart;
    }

    public Statement getStatement() {
        return Statement;
    }

    public void setStatement(Statement Statement) {
        this.Statement=Statement;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ForeachName!=null) ForeachName.accept(visitor);
        if(Designator!=null) Designator.accept(visitor);
        if(ForStart!=null) ForStart.accept(visitor);
        if(Statement!=null) Statement.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ForeachName!=null) ForeachName.traverseTopDown(visitor);
        if(Designator!=null) Designator.traverseTopDown(visitor);
        if(ForStart!=null) ForStart.traverseTopDown(visitor);
        if(Statement!=null) Statement.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ForeachName!=null) ForeachName.traverseBottomUp(visitor);
        if(Designator!=null) Designator.traverseBottomUp(visitor);
        if(ForStart!=null) ForStart.traverseBottomUp(visitor);
        if(Statement!=null) Statement.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ForeachStatement(\n");

        if(ForeachName!=null)
            buffer.append(ForeachName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+I2);
        buffer.append("\n");

        if(Designator!=null)
            buffer.append(Designator.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForStart!=null)
            buffer.append(ForStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Statement!=null)
            buffer.append(Statement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ForeachStatement]");
        return buffer.toString();
    }
}
