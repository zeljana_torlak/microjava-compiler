// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassName implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private String abstClassName;

    public AbstractClassName (String abstClassName) {
        this.abstClassName=abstClassName;
    }

    public String getAbstClassName() {
        return abstClassName;
    }

    public void setAbstClassName(String abstClassName) {
        this.abstClassName=abstClassName;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassName(\n");

        buffer.append(" "+tab+abstClassName);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassName]");
        return buffer.toString();
    }
}
