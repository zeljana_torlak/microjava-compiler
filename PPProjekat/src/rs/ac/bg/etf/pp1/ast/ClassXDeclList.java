// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ClassXDeclList extends XDeclList {

    private ClassDecl ClassDecl;
    private XDeclList XDeclList;

    public ClassXDeclList (ClassDecl ClassDecl, XDeclList XDeclList) {
        this.ClassDecl=ClassDecl;
        if(ClassDecl!=null) ClassDecl.setParent(this);
        this.XDeclList=XDeclList;
        if(XDeclList!=null) XDeclList.setParent(this);
    }

    public ClassDecl getClassDecl() {
        return ClassDecl;
    }

    public void setClassDecl(ClassDecl ClassDecl) {
        this.ClassDecl=ClassDecl;
    }

    public XDeclList getXDeclList() {
        return XDeclList;
    }

    public void setXDeclList(XDeclList XDeclList) {
        this.XDeclList=XDeclList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDecl!=null) ClassDecl.accept(visitor);
        if(XDeclList!=null) XDeclList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDecl!=null) ClassDecl.traverseTopDown(visitor);
        if(XDeclList!=null) XDeclList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDecl!=null) ClassDecl.traverseBottomUp(visitor);
        if(XDeclList!=null) XDeclList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassXDeclList(\n");

        if(ClassDecl!=null)
            buffer.append(ClassDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(XDeclList!=null)
            buffer.append(XDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassXDeclList]");
        return buffer.toString();
    }
}
