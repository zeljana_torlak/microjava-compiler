// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class TermExtListS extends TermExtList {

    private Mulop Mulop;
    private Factor Factor;
    private TermEnd TermEnd;
    private TermExtList TermExtList;

    public TermExtListS (Mulop Mulop, Factor Factor, TermEnd TermEnd, TermExtList TermExtList) {
        this.Mulop=Mulop;
        if(Mulop!=null) Mulop.setParent(this);
        this.Factor=Factor;
        if(Factor!=null) Factor.setParent(this);
        this.TermEnd=TermEnd;
        if(TermEnd!=null) TermEnd.setParent(this);
        this.TermExtList=TermExtList;
        if(TermExtList!=null) TermExtList.setParent(this);
    }

    public Mulop getMulop() {
        return Mulop;
    }

    public void setMulop(Mulop Mulop) {
        this.Mulop=Mulop;
    }

    public Factor getFactor() {
        return Factor;
    }

    public void setFactor(Factor Factor) {
        this.Factor=Factor;
    }

    public TermEnd getTermEnd() {
        return TermEnd;
    }

    public void setTermEnd(TermEnd TermEnd) {
        this.TermEnd=TermEnd;
    }

    public TermExtList getTermExtList() {
        return TermExtList;
    }

    public void setTermExtList(TermExtList TermExtList) {
        this.TermExtList=TermExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Mulop!=null) Mulop.accept(visitor);
        if(Factor!=null) Factor.accept(visitor);
        if(TermEnd!=null) TermEnd.accept(visitor);
        if(TermExtList!=null) TermExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Mulop!=null) Mulop.traverseTopDown(visitor);
        if(Factor!=null) Factor.traverseTopDown(visitor);
        if(TermEnd!=null) TermEnd.traverseTopDown(visitor);
        if(TermExtList!=null) TermExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Mulop!=null) Mulop.traverseBottomUp(visitor);
        if(Factor!=null) Factor.traverseBottomUp(visitor);
        if(TermEnd!=null) TermEnd.traverseBottomUp(visitor);
        if(TermExtList!=null) TermExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("TermExtListS(\n");

        if(Mulop!=null)
            buffer.append(Mulop.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Factor!=null)
            buffer.append(Factor.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(TermEnd!=null)
            buffer.append(TermEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(TermExtList!=null)
            buffer.append(TermExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [TermExtListS]");
        return buffer.toString();
    }
}
