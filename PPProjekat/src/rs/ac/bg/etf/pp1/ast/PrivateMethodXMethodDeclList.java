// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class PrivateMethodXMethodDeclList extends XMethodDeclAccessList {

    private MethodDecl MethodDecl;
    private XMethodDeclAccessList XMethodDeclAccessList;

    public PrivateMethodXMethodDeclList (MethodDecl MethodDecl, XMethodDeclAccessList XMethodDeclAccessList) {
        this.MethodDecl=MethodDecl;
        if(MethodDecl!=null) MethodDecl.setParent(this);
        this.XMethodDeclAccessList=XMethodDeclAccessList;
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.setParent(this);
    }

    public MethodDecl getMethodDecl() {
        return MethodDecl;
    }

    public void setMethodDecl(MethodDecl MethodDecl) {
        this.MethodDecl=MethodDecl;
    }

    public XMethodDeclAccessList getXMethodDeclAccessList() {
        return XMethodDeclAccessList;
    }

    public void setXMethodDeclAccessList(XMethodDeclAccessList XMethodDeclAccessList) {
        this.XMethodDeclAccessList=XMethodDeclAccessList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodDecl!=null) MethodDecl.accept(visitor);
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodDecl!=null) MethodDecl.traverseTopDown(visitor);
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodDecl!=null) MethodDecl.traverseBottomUp(visitor);
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PrivateMethodXMethodDeclList(\n");

        if(MethodDecl!=null)
            buffer.append(MethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(XMethodDeclAccessList!=null)
            buffer.append(XMethodDeclAccessList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PrivateMethodXMethodDeclList]");
        return buffer.toString();
    }
}
