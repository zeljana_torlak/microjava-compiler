// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ConstDecl implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private TypeName TypeName;
    private String constName;
    private XConst XConst;
    private ConstDeclExtList ConstDeclExtList;

    public ConstDecl (TypeName TypeName, String constName, XConst XConst, ConstDeclExtList ConstDeclExtList) {
        this.TypeName=TypeName;
        if(TypeName!=null) TypeName.setParent(this);
        this.constName=constName;
        this.XConst=XConst;
        if(XConst!=null) XConst.setParent(this);
        this.ConstDeclExtList=ConstDeclExtList;
        if(ConstDeclExtList!=null) ConstDeclExtList.setParent(this);
    }

    public TypeName getTypeName() {
        return TypeName;
    }

    public void setTypeName(TypeName TypeName) {
        this.TypeName=TypeName;
    }

    public String getConstName() {
        return constName;
    }

    public void setConstName(String constName) {
        this.constName=constName;
    }

    public XConst getXConst() {
        return XConst;
    }

    public void setXConst(XConst XConst) {
        this.XConst=XConst;
    }

    public ConstDeclExtList getConstDeclExtList() {
        return ConstDeclExtList;
    }

    public void setConstDeclExtList(ConstDeclExtList ConstDeclExtList) {
        this.ConstDeclExtList=ConstDeclExtList;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(TypeName!=null) TypeName.accept(visitor);
        if(XConst!=null) XConst.accept(visitor);
        if(ConstDeclExtList!=null) ConstDeclExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(TypeName!=null) TypeName.traverseTopDown(visitor);
        if(XConst!=null) XConst.traverseTopDown(visitor);
        if(ConstDeclExtList!=null) ConstDeclExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(TypeName!=null) TypeName.traverseBottomUp(visitor);
        if(XConst!=null) XConst.traverseBottomUp(visitor);
        if(ConstDeclExtList!=null) ConstDeclExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstDecl(\n");

        if(TypeName!=null)
            buffer.append(TypeName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+constName);
        buffer.append("\n");

        if(XConst!=null)
            buffer.append(XConst.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstDeclExtList!=null)
            buffer.append(ConstDeclExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstDecl]");
        return buffer.toString();
    }
}
