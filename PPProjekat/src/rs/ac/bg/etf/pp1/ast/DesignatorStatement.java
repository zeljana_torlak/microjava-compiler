// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class DesignatorStatement implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private Designator Designator;
    private XDesignatorStatementExt XDesignatorStatementExt;

    public DesignatorStatement (Designator Designator, XDesignatorStatementExt XDesignatorStatementExt) {
        this.Designator=Designator;
        if(Designator!=null) Designator.setParent(this);
        this.XDesignatorStatementExt=XDesignatorStatementExt;
        if(XDesignatorStatementExt!=null) XDesignatorStatementExt.setParent(this);
    }

    public Designator getDesignator() {
        return Designator;
    }

    public void setDesignator(Designator Designator) {
        this.Designator=Designator;
    }

    public XDesignatorStatementExt getXDesignatorStatementExt() {
        return XDesignatorStatementExt;
    }

    public void setXDesignatorStatementExt(XDesignatorStatementExt XDesignatorStatementExt) {
        this.XDesignatorStatementExt=XDesignatorStatementExt;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Designator!=null) Designator.accept(visitor);
        if(XDesignatorStatementExt!=null) XDesignatorStatementExt.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Designator!=null) Designator.traverseTopDown(visitor);
        if(XDesignatorStatementExt!=null) XDesignatorStatementExt.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Designator!=null) Designator.traverseBottomUp(visitor);
        if(XDesignatorStatementExt!=null) XDesignatorStatementExt.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorStatement(\n");

        if(Designator!=null)
            buffer.append(Designator.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(XDesignatorStatementExt!=null)
            buffer.append(XDesignatorStatementExt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorStatement]");
        return buffer.toString();
    }
}
