// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class Designator implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private DesignatorStart DesignatorStart;
    private String desName;
    private DesignatorEnd DesignatorEnd;
    private XDesignatorExtList XDesignatorExtList;

    public Designator (DesignatorStart DesignatorStart, String desName, DesignatorEnd DesignatorEnd, XDesignatorExtList XDesignatorExtList) {
        this.DesignatorStart=DesignatorStart;
        if(DesignatorStart!=null) DesignatorStart.setParent(this);
        this.desName=desName;
        this.DesignatorEnd=DesignatorEnd;
        if(DesignatorEnd!=null) DesignatorEnd.setParent(this);
        this.XDesignatorExtList=XDesignatorExtList;
        if(XDesignatorExtList!=null) XDesignatorExtList.setParent(this);
    }

    public DesignatorStart getDesignatorStart() {
        return DesignatorStart;
    }

    public void setDesignatorStart(DesignatorStart DesignatorStart) {
        this.DesignatorStart=DesignatorStart;
    }

    public String getDesName() {
        return desName;
    }

    public void setDesName(String desName) {
        this.desName=desName;
    }

    public DesignatorEnd getDesignatorEnd() {
        return DesignatorEnd;
    }

    public void setDesignatorEnd(DesignatorEnd DesignatorEnd) {
        this.DesignatorEnd=DesignatorEnd;
    }

    public XDesignatorExtList getXDesignatorExtList() {
        return XDesignatorExtList;
    }

    public void setXDesignatorExtList(XDesignatorExtList XDesignatorExtList) {
        this.XDesignatorExtList=XDesignatorExtList;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorStart!=null) DesignatorStart.accept(visitor);
        if(DesignatorEnd!=null) DesignatorEnd.accept(visitor);
        if(XDesignatorExtList!=null) XDesignatorExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorStart!=null) DesignatorStart.traverseTopDown(visitor);
        if(DesignatorEnd!=null) DesignatorEnd.traverseTopDown(visitor);
        if(XDesignatorExtList!=null) XDesignatorExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorStart!=null) DesignatorStart.traverseBottomUp(visitor);
        if(DesignatorEnd!=null) DesignatorEnd.traverseBottomUp(visitor);
        if(XDesignatorExtList!=null) XDesignatorExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("Designator(\n");

        if(DesignatorStart!=null)
            buffer.append(DesignatorStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+desName);
        buffer.append("\n");

        if(DesignatorEnd!=null)
            buffer.append(DesignatorEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(XDesignatorExtList!=null)
            buffer.append(XDesignatorExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [Designator]");
        return buffer.toString();
    }
}
