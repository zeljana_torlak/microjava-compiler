// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ConstDeclExtListS extends ConstDeclExtList {

    private String constName;
    private XConst XConst;
    private ConstDeclExtList ConstDeclExtList;

    public ConstDeclExtListS (String constName, XConst XConst, ConstDeclExtList ConstDeclExtList) {
        this.constName=constName;
        this.XConst=XConst;
        if(XConst!=null) XConst.setParent(this);
        this.ConstDeclExtList=ConstDeclExtList;
        if(ConstDeclExtList!=null) ConstDeclExtList.setParent(this);
    }

    public String getConstName() {
        return constName;
    }

    public void setConstName(String constName) {
        this.constName=constName;
    }

    public XConst getXConst() {
        return XConst;
    }

    public void setXConst(XConst XConst) {
        this.XConst=XConst;
    }

    public ConstDeclExtList getConstDeclExtList() {
        return ConstDeclExtList;
    }

    public void setConstDeclExtList(ConstDeclExtList ConstDeclExtList) {
        this.ConstDeclExtList=ConstDeclExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(XConst!=null) XConst.accept(visitor);
        if(ConstDeclExtList!=null) ConstDeclExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(XConst!=null) XConst.traverseTopDown(visitor);
        if(ConstDeclExtList!=null) ConstDeclExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(XConst!=null) XConst.traverseBottomUp(visitor);
        if(ConstDeclExtList!=null) ConstDeclExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstDeclExtListS(\n");

        buffer.append(" "+tab+constName);
        buffer.append("\n");

        if(XConst!=null)
            buffer.append(XConst.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstDeclExtList!=null)
            buffer.append(ConstDeclExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstDeclExtListS]");
        return buffer.toString();
    }
}
