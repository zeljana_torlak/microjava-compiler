// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(ActParsOpt ActParsOpt) { }
    public void visit(FormPars FormPars) { }
    public void visit(Factor Factor) { }
    public void visit(Statement Statement) { }
    public void visit(ExprNumConstOpt ExprNumConstOpt) { }
    public void visit(XType XType) { }
    public void visit(AddopLeft AddopLeft) { }
    public void visit(XMethodDeclAccessList XMethodDeclAccessList) { }
    public void visit(DesignatorStatementOpt DesignatorStatementOpt) { }
    public void visit(AddopRight AddopRight) { }
    public void visit(ConditionExtList ConditionExtList) { }
    public void visit(ActParsExtList ActParsExtList) { }
    public void visit(SubOpt SubOpt) { }
    public void visit(Relop Relop) { }
    public void visit(MulopRight MulopRight) { }
    public void visit(XDesignatorStatementExt XDesignatorStatementExt) { }
    public void visit(VarDeclExtList VarDeclExtList) { }
    public void visit(SqrBraceOpt SqrBraceOpt) { }
    public void visit(MulopLeft MulopLeft) { }
    public void visit(XConst XConst) { }
    public void visit(XMethodDeclListOpt XMethodDeclListOpt) { }
    public void visit(ConditionOpt ConditionOpt) { }
    public void visit(FormParsExtList FormParsExtList) { }
    public void visit(VarDeclAccessList VarDeclAccessList) { }
    public void visit(VarDecl VarDecl) { }
    public void visit(ExprExtList ExprExtList) { }
    public void visit(ConstDeclExtList ConstDeclExtList) { }
    public void visit(XDeclList XDeclList) { }
    public void visit(FactorExprExtOpt FactorExprExtOpt) { }
    public void visit(Mulop Mulop) { }
    public void visit(Addop Addop) { }
    public void visit(StatementList StatementList) { }
    public void visit(Assignop Assignop) { }
    public void visit(CondFactExtOpt CondFactExtOpt) { }
    public void visit(FormParsOpt FormParsOpt) { }
    public void visit(CondTermExtList CondTermExtList) { }
    public void visit(ClassDecl ClassDecl) { }
    public void visit(MethodDeclListOpt MethodDeclListOpt) { }
    public void visit(MethodDeclList MethodDeclList) { }
    public void visit(TermExtList TermExtList) { }
    public void visit(ExtendsOpt ExtendsOpt) { }
    public void visit(XDesignatorExtList XDesignatorExtList) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(MethodDeclAccessList MethodDeclAccessList) { }
    public void visit(ExprOpt ExprOpt) { }
    public void visit(FactorActParsOpt FactorActParsOpt) { }
    public void visit(ModassignMulopRight ModassignMulopRight) { visit(); }
    public void visit(DivassignMulopRight DivassignMulopRight) { visit(); }
    public void visit(MulassignMulopRight MulassignMulopRight) { visit(); }
    public void visit(ModMulopLeft ModMulopLeft) { visit(); }
    public void visit(DivMulopLeft DivMulopLeft) { visit(); }
    public void visit(MulMulopLeft MulMulopLeft) { visit(); }
    public void visit(MulopMulopRight MulopMulopRight) { visit(); }
    public void visit(MulopMulopLeft MulopMulopLeft) { visit(); }
    public void visit(SubassignAddopRight SubassignAddopRight) { visit(); }
    public void visit(AddassignAddopRight AddassignAddopRight) { visit(); }
    public void visit(SubAddopLeft SubAddopLeft) { visit(); }
    public void visit(AddAddopLeft AddAddopLeft) { visit(); }
    public void visit(AddopAddopRight AddopAddopRight) { visit(); }
    public void visit(AddopAddopLeft AddopAddopLeft) { visit(); }
    public void visit(LeRelop LeRelop) { visit(); }
    public void visit(LtRelop LtRelop) { visit(); }
    public void visit(GeRelop GeRelop) { visit(); }
    public void visit(GtRelop GtRelop) { visit(); }
    public void visit(NeqRelop NeqRelop) { visit(); }
    public void visit(EqRelop EqRelop) { visit(); }
    public void visit(MulopRightAssignop MulopRightAssignop) { visit(); }
    public void visit(AddopRightAssignop AddopRightAssignop) { visit(); }
    public void visit(AssignAssignop AssignAssignop) { visit(); }
    public void visit(DesignatorEnd DesignatorEnd) { visit(); }
    public void visit(DesignatorStart DesignatorStart) { visit(); }
    public void visit(NoXDesignatorExtList NoXDesignatorExtList) { visit(); }
    public void visit(SqrBraceXDesignatorExtList SqrBraceXDesignatorExtList) { visit(); }
    public void visit(DotXDesignatorExtList DotXDesignatorExtList) { visit(); }
    public void visit(Designator Designator) { visit(); }
    public void visit(NoFactorExprExtOpt NoFactorExprExtOpt) { visit(); }
    public void visit(WithFactorExprExtOpt WithFactorExprExtOpt) { visit(); }
    public void visit(NoFactorActParsOpt NoFactorActParsOpt) { visit(); }
    public void visit(WithFactorActParsOpt WithFactorActParsOpt) { visit(); }
    public void visit(ParenFactor ParenFactor) { visit(); }
    public void visit(NewFactor NewFactor) { visit(); }
    public void visit(FalseConstFactor FalseConstFactor) { visit(); }
    public void visit(TrueConstFactor TrueConstFactor) { visit(); }
    public void visit(CharConstFactor CharConstFactor) { visit(); }
    public void visit(NumConstFactor NumConstFactor) { visit(); }
    public void visit(DesignFactor DesignFactor) { visit(); }
    public void visit(TermEnd TermEnd) { visit(); }
    public void visit(NoTermExtList NoTermExtList) { visit(); }
    public void visit(TermExtListS TermExtListS) { visit(); }
    public void visit(Term Term) { visit(); }
    public void visit(ExprEnd ExprEnd) { visit(); }
    public void visit(NoExprExtList NoExprExtList) { visit(); }
    public void visit(ExprExtListS ExprExtListS) { visit(); }
    public void visit(ExprStart ExprStart) { visit(); }
    public void visit(NoSubOpt NoSubOpt) { visit(); }
    public void visit(WithSubOpt WithSubOpt) { visit(); }
    public void visit(Expr Expr) { visit(); }
    public void visit(NoCondFactExtOpt NoCondFactExtOpt) { visit(); }
    public void visit(WithCondFactExtOpt WithCondFactExtOpt) { visit(); }
    public void visit(CondFact CondFact) { visit(); }
    public void visit(NoCondTermExtList NoCondTermExtList) { visit(); }
    public void visit(CondTermExtListS CondTermExtListS) { visit(); }
    public void visit(CondTerm CondTerm) { visit(); }
    public void visit(CondEnd CondEnd) { visit(); }
    public void visit(NoConditionExtList NoConditionExtList) { visit(); }
    public void visit(ConditionExtListS ConditionExtListS) { visit(); }
    public void visit(Condition Condition) { visit(); }
    public void visit(NoActParsExtList NoActParsExtList) { visit(); }
    public void visit(ActParsExtListS ActParsExtListS) { visit(); }
    public void visit(ActParsEnd ActParsEnd) { visit(); }
    public void visit(ActParsStart ActParsStart) { visit(); }
    public void visit(ActPars ActPars) { visit(); }
    public void visit(NoActParsOpt NoActParsOpt) { visit(); }
    public void visit(WithActParsOpt WithActParsOpt) { visit(); }
    public void visit(DecrXDesignatorStatementExt DecrXDesignatorStatementExt) { visit(); }
    public void visit(IncrXDesignatorStatementExt IncrXDesignatorStatementExt) { visit(); }
    public void visit(ActParsXDesignatorStatementExt ActParsXDesignatorStatementExt) { visit(); }
    public void visit(AssignopXDesignatorStatementExt AssignopXDesignatorStatementExt) { visit(); }
    public void visit(DesignatorStatement DesignatorStatement) { visit(); }
    public void visit(NoExprNumConstOpt NoExprNumConstOpt) { visit(); }
    public void visit(WithExprNumConstOpt WithExprNumConstOpt) { visit(); }
    public void visit(NoExprOpt NoExprOpt) { visit(); }
    public void visit(WithExprOpt WithExprOpt) { visit(); }
    public void visit(NoConditionOpt NoConditionOpt) { visit(); }
    public void visit(ErrorConditionOpt ErrorConditionOpt) { visit(); }
    public void visit(WithConditionOpt WithConditionOpt) { visit(); }
    public void visit(NoDesignatorStatementOpt NoDesignatorStatementOpt) { visit(); }
    public void visit(WithDesignatorStatementOpt WithDesignatorStatementOpt) { visit(); }
    public void visit(ForStart ForStart) { visit(); }
    public void visit(AfterForStart AfterForStart) { visit(); }
    public void visit(CondForStart CondForStart) { visit(); }
    public void visit(ElseStart ElseStart) { visit(); }
    public void visit(ForeachName ForeachName) { visit(); }
    public void visit(ForName ForName) { visit(); }
    public void visit(BlockStatement BlockStatement) { visit(); }
    public void visit(PrintStatement PrintStatement) { visit(); }
    public void visit(ReadStatement ReadStatement) { visit(); }
    public void visit(ReturnStatement ReturnStatement) { visit(); }
    public void visit(ContinueStatement ContinueStatement) { visit(); }
    public void visit(BreakStatement BreakStatement) { visit(); }
    public void visit(ForeachStatement ForeachStatement) { visit(); }
    public void visit(ForStatement ForStatement) { visit(); }
    public void visit(IfStatement IfStatement) { visit(); }
    public void visit(IfElseStatement IfElseStatement) { visit(); }
    public void visit(ErrorStatement ErrorStatement) { visit(); }
    public void visit(DesignStatement DesignStatement) { visit(); }
    public void visit(Type Type) { visit(); }
    public void visit(NoFormParsExtList NoFormParsExtList) { visit(); }
    public void visit(ErrorFormParsExtList ErrorFormParsExtList) { visit(); }
    public void visit(FormParsExtListS FormParsExtListS) { visit(); }
    public void visit(ErrorFormPars ErrorFormPars) { visit(); }
    public void visit(SFormPars SFormPars) { visit(); }
    public void visit(AbstractMethodTypeName AbstractMethodTypeName) { visit(); }
    public void visit(AbstractMethodDecl AbstractMethodDecl) { visit(); }
    public void visit(NoStatementList NoStatementList) { visit(); }
    public void visit(StatementListS StatementListS) { visit(); }
    public void visit(NoFormParsOpt NoFormParsOpt) { visit(); }
    public void visit(WithFormParsOpt WithFormParsOpt) { visit(); }
    public void visit(VoidXType VoidXType) { visit(); }
    public void visit(TypeXType TypeXType) { visit(); }
    public void visit(MethodTypeName MethodTypeName) { visit(); }
    public void visit(MethodDecl MethodDecl) { visit(); }
    public void visit(NoXMethodDeclList NoXMethodDeclList) { visit(); }
    public void visit(ErrorAbstractXMethodDeclList ErrorAbstractXMethodDeclList) { visit(); }
    public void visit(PublicAbstractXMethodDeclList PublicAbstractXMethodDeclList) { visit(); }
    public void visit(ProtectedAbstractXMethodDeclList ProtectedAbstractXMethodDeclList) { visit(); }
    public void visit(PrivateAbstractXMethodDeclList PrivateAbstractXMethodDeclList) { visit(); }
    public void visit(PublicMethodXMethodDeclList PublicMethodXMethodDeclList) { visit(); }
    public void visit(ProtectedMethodXMethodDeclList ProtectedMethodXMethodDeclList) { visit(); }
    public void visit(PrivateMethodXMethodDeclList PrivateMethodXMethodDeclList) { visit(); }
    public void visit(NoXMethodDeclListOpt NoXMethodDeclListOpt) { visit(); }
    public void visit(WithXMethodDeclListOpt WithXMethodDeclListOpt) { visit(); }
    public void visit(AbstractClassName AbstractClassName) { visit(); }
    public void visit(AbstractClassDecl AbstractClassDecl) { visit(); }
    public void visit(NoMethodDeclListOpt NoMethodDeclListOpt) { visit(); }
    public void visit(WithMethodDeclListOpt WithMethodDeclListOpt) { visit(); }
    public void visit(NoVarDeclList NoVarDeclList) { visit(); }
    public void visit(VarDeclListS VarDeclListS) { visit(); }
    public void visit(NoVarDeclAccessList NoVarDeclAccessList) { visit(); }
    public void visit(PublicVarDeclAccessListS PublicVarDeclAccessListS) { visit(); }
    public void visit(ProtectedVarDeclAccessListS ProtectedVarDeclAccessListS) { visit(); }
    public void visit(PrivateVarDeclAccessListS PrivateVarDeclAccessListS) { visit(); }
    public void visit(NoExtendsOpt NoExtendsOpt) { visit(); }
    public void visit(WithExtendsOpt WithExtendsOpt) { visit(); }
    public void visit(ClassName ClassName) { visit(); }
    public void visit(ErrorClassDecl ErrorClassDecl) { visit(); }
    public void visit(SClassDecl SClassDecl) { visit(); }
    public void visit(NoVarDeclExtList NoVarDeclExtList) { visit(); }
    public void visit(ErrorVarDeclExtList ErrorVarDeclExtList) { visit(); }
    public void visit(VarDeclExtListS VarDeclExtListS) { visit(); }
    public void visit(NoSqrBraceOpt NoSqrBraceOpt) { visit(); }
    public void visit(WithSqrBraceOpt WithSqrBraceOpt) { visit(); }
    public void visit(TypeName TypeName) { visit(); }
    public void visit(ErrorVarDecl ErrorVarDecl) { visit(); }
    public void visit(SVarDecl SVarDecl) { visit(); }
    public void visit(NoConstDeclExtList NoConstDeclExtList) { visit(); }
    public void visit(ConstDeclExtListS ConstDeclExtListS) { visit(); }
    public void visit(FalseXConst FalseXConst) { visit(); }
    public void visit(TrueXConst TrueXConst) { visit(); }
    public void visit(CharXConst CharXConst) { visit(); }
    public void visit(NumXConst NumXConst) { visit(); }
    public void visit(ConstDecl ConstDecl) { visit(); }
    public void visit(NoMethodDeclList NoMethodDeclList) { visit(); }
    public void visit(MethodDeclListS MethodDeclListS) { visit(); }
    public void visit(NoMethodDeclAccessListt NoMethodDeclAccessListt) { visit(); }
    public void visit(PublicMethodDeclAccessListS PublicMethodDeclAccessListS) { visit(); }
    public void visit(ProtectedMethodDeclAccessListS ProtectedMethodDeclAccessListS) { visit(); }
    public void visit(PrivateMethodDeclAccessListS PrivateMethodDeclAccessListS) { visit(); }
    public void visit(NoXDeclList NoXDeclList) { visit(); }
    public void visit(ClassXDeclList ClassXDeclList) { visit(); }
    public void visit(AbstractClassXDeclList AbstractClassXDeclList) { visit(); }
    public void visit(VarXDeclList VarXDeclList) { visit(); }
    public void visit(ConstXDeclList ConstXDeclList) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
