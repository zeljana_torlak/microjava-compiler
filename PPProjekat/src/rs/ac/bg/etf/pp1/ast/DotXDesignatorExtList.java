// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class DotXDesignatorExtList extends XDesignatorExtList {

    private String dotDesName;
    private DesignatorEnd DesignatorEnd;
    private XDesignatorExtList XDesignatorExtList;

    public DotXDesignatorExtList (String dotDesName, DesignatorEnd DesignatorEnd, XDesignatorExtList XDesignatorExtList) {
        this.dotDesName=dotDesName;
        this.DesignatorEnd=DesignatorEnd;
        if(DesignatorEnd!=null) DesignatorEnd.setParent(this);
        this.XDesignatorExtList=XDesignatorExtList;
        if(XDesignatorExtList!=null) XDesignatorExtList.setParent(this);
    }

    public String getDotDesName() {
        return dotDesName;
    }

    public void setDotDesName(String dotDesName) {
        this.dotDesName=dotDesName;
    }

    public DesignatorEnd getDesignatorEnd() {
        return DesignatorEnd;
    }

    public void setDesignatorEnd(DesignatorEnd DesignatorEnd) {
        this.DesignatorEnd=DesignatorEnd;
    }

    public XDesignatorExtList getXDesignatorExtList() {
        return XDesignatorExtList;
    }

    public void setXDesignatorExtList(XDesignatorExtList XDesignatorExtList) {
        this.XDesignatorExtList=XDesignatorExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorEnd!=null) DesignatorEnd.accept(visitor);
        if(XDesignatorExtList!=null) XDesignatorExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorEnd!=null) DesignatorEnd.traverseTopDown(visitor);
        if(XDesignatorExtList!=null) XDesignatorExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorEnd!=null) DesignatorEnd.traverseBottomUp(visitor);
        if(XDesignatorExtList!=null) XDesignatorExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DotXDesignatorExtList(\n");

        buffer.append(" "+tab+dotDesName);
        buffer.append("\n");

        if(DesignatorEnd!=null)
            buffer.append(DesignatorEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(XDesignatorExtList!=null)
            buffer.append(XDesignatorExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DotXDesignatorExtList]");
        return buffer.toString();
    }
}
