// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ActParsExtListS extends ActParsExtList {

    private Expr Expr;
    private ActParsEnd ActParsEnd;
    private ActParsExtList ActParsExtList;

    public ActParsExtListS (Expr Expr, ActParsEnd ActParsEnd, ActParsExtList ActParsExtList) {
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
        this.ActParsEnd=ActParsEnd;
        if(ActParsEnd!=null) ActParsEnd.setParent(this);
        this.ActParsExtList=ActParsExtList;
        if(ActParsExtList!=null) ActParsExtList.setParent(this);
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public ActParsEnd getActParsEnd() {
        return ActParsEnd;
    }

    public void setActParsEnd(ActParsEnd ActParsEnd) {
        this.ActParsEnd=ActParsEnd;
    }

    public ActParsExtList getActParsExtList() {
        return ActParsExtList;
    }

    public void setActParsExtList(ActParsExtList ActParsExtList) {
        this.ActParsExtList=ActParsExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Expr!=null) Expr.accept(visitor);
        if(ActParsEnd!=null) ActParsEnd.accept(visitor);
        if(ActParsExtList!=null) ActParsExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
        if(ActParsEnd!=null) ActParsEnd.traverseTopDown(visitor);
        if(ActParsExtList!=null) ActParsExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        if(ActParsEnd!=null) ActParsEnd.traverseBottomUp(visitor);
        if(ActParsExtList!=null) ActParsExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ActParsExtListS(\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ActParsEnd!=null)
            buffer.append(ActParsEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ActParsExtList!=null)
            buffer.append(ActParsExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ActParsExtListS]");
        return buffer.toString();
    }
}
