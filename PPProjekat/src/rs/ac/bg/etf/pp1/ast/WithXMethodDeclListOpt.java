// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class WithXMethodDeclListOpt extends XMethodDeclListOpt {

    private XMethodDeclAccessList XMethodDeclAccessList;

    public WithXMethodDeclListOpt (XMethodDeclAccessList XMethodDeclAccessList) {
        this.XMethodDeclAccessList=XMethodDeclAccessList;
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.setParent(this);
    }

    public XMethodDeclAccessList getXMethodDeclAccessList() {
        return XMethodDeclAccessList;
    }

    public void setXMethodDeclAccessList(XMethodDeclAccessList XMethodDeclAccessList) {
        this.XMethodDeclAccessList=XMethodDeclAccessList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(XMethodDeclAccessList!=null) XMethodDeclAccessList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("WithXMethodDeclListOpt(\n");

        if(XMethodDeclAccessList!=null)
            buffer.append(XMethodDeclAccessList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [WithXMethodDeclListOpt]");
        return buffer.toString();
    }
}
