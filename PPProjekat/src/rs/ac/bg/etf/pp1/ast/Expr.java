// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class Expr implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private ExprStart ExprStart;
    private SubOpt SubOpt;
    private ExprExtList ExprExtList;

    public Expr (ExprStart ExprStart, SubOpt SubOpt, ExprExtList ExprExtList) {
        this.ExprStart=ExprStart;
        if(ExprStart!=null) ExprStart.setParent(this);
        this.SubOpt=SubOpt;
        if(SubOpt!=null) SubOpt.setParent(this);
        this.ExprExtList=ExprExtList;
        if(ExprExtList!=null) ExprExtList.setParent(this);
    }

    public ExprStart getExprStart() {
        return ExprStart;
    }

    public void setExprStart(ExprStart ExprStart) {
        this.ExprStart=ExprStart;
    }

    public SubOpt getSubOpt() {
        return SubOpt;
    }

    public void setSubOpt(SubOpt SubOpt) {
        this.SubOpt=SubOpt;
    }

    public ExprExtList getExprExtList() {
        return ExprExtList;
    }

    public void setExprExtList(ExprExtList ExprExtList) {
        this.ExprExtList=ExprExtList;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ExprStart!=null) ExprStart.accept(visitor);
        if(SubOpt!=null) SubOpt.accept(visitor);
        if(ExprExtList!=null) ExprExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ExprStart!=null) ExprStart.traverseTopDown(visitor);
        if(SubOpt!=null) SubOpt.traverseTopDown(visitor);
        if(ExprExtList!=null) ExprExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ExprStart!=null) ExprStart.traverseBottomUp(visitor);
        if(SubOpt!=null) SubOpt.traverseBottomUp(visitor);
        if(ExprExtList!=null) ExprExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("Expr(\n");

        if(ExprStart!=null)
            buffer.append(ExprStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(SubOpt!=null)
            buffer.append(SubOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ExprExtList!=null)
            buffer.append(ExprExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [Expr]");
        return buffer.toString();
    }
}
