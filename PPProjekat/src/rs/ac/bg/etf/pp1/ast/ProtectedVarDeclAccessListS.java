// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ProtectedVarDeclAccessListS extends VarDeclAccessList {

    private VarDecl VarDecl;
    private VarDeclAccessList VarDeclAccessList;

    public ProtectedVarDeclAccessListS (VarDecl VarDecl, VarDeclAccessList VarDeclAccessList) {
        this.VarDecl=VarDecl;
        if(VarDecl!=null) VarDecl.setParent(this);
        this.VarDeclAccessList=VarDeclAccessList;
        if(VarDeclAccessList!=null) VarDeclAccessList.setParent(this);
    }

    public VarDecl getVarDecl() {
        return VarDecl;
    }

    public void setVarDecl(VarDecl VarDecl) {
        this.VarDecl=VarDecl;
    }

    public VarDeclAccessList getVarDeclAccessList() {
        return VarDeclAccessList;
    }

    public void setVarDeclAccessList(VarDeclAccessList VarDeclAccessList) {
        this.VarDeclAccessList=VarDeclAccessList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDecl!=null) VarDecl.accept(visitor);
        if(VarDeclAccessList!=null) VarDeclAccessList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDecl!=null) VarDecl.traverseTopDown(visitor);
        if(VarDeclAccessList!=null) VarDeclAccessList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDecl!=null) VarDecl.traverseBottomUp(visitor);
        if(VarDeclAccessList!=null) VarDeclAccessList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ProtectedVarDeclAccessListS(\n");

        if(VarDecl!=null)
            buffer.append(VarDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclAccessList!=null)
            buffer.append(VarDeclAccessList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ProtectedVarDeclAccessListS]");
        return buffer.toString();
    }
}
