// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class SVarDecl extends VarDecl {

    private TypeName TypeName;
    private String varName;
    private SqrBraceOpt SqrBraceOpt;
    private VarDeclExtList VarDeclExtList;

    public SVarDecl (TypeName TypeName, String varName, SqrBraceOpt SqrBraceOpt, VarDeclExtList VarDeclExtList) {
        this.TypeName=TypeName;
        if(TypeName!=null) TypeName.setParent(this);
        this.varName=varName;
        this.SqrBraceOpt=SqrBraceOpt;
        if(SqrBraceOpt!=null) SqrBraceOpt.setParent(this);
        this.VarDeclExtList=VarDeclExtList;
        if(VarDeclExtList!=null) VarDeclExtList.setParent(this);
    }

    public TypeName getTypeName() {
        return TypeName;
    }

    public void setTypeName(TypeName TypeName) {
        this.TypeName=TypeName;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName=varName;
    }

    public SqrBraceOpt getSqrBraceOpt() {
        return SqrBraceOpt;
    }

    public void setSqrBraceOpt(SqrBraceOpt SqrBraceOpt) {
        this.SqrBraceOpt=SqrBraceOpt;
    }

    public VarDeclExtList getVarDeclExtList() {
        return VarDeclExtList;
    }

    public void setVarDeclExtList(VarDeclExtList VarDeclExtList) {
        this.VarDeclExtList=VarDeclExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(TypeName!=null) TypeName.accept(visitor);
        if(SqrBraceOpt!=null) SqrBraceOpt.accept(visitor);
        if(VarDeclExtList!=null) VarDeclExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(TypeName!=null) TypeName.traverseTopDown(visitor);
        if(SqrBraceOpt!=null) SqrBraceOpt.traverseTopDown(visitor);
        if(VarDeclExtList!=null) VarDeclExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(TypeName!=null) TypeName.traverseBottomUp(visitor);
        if(SqrBraceOpt!=null) SqrBraceOpt.traverseBottomUp(visitor);
        if(VarDeclExtList!=null) VarDeclExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("SVarDecl(\n");

        if(TypeName!=null)
            buffer.append(TypeName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+varName);
        buffer.append("\n");

        if(SqrBraceOpt!=null)
            buffer.append(SqrBraceOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclExtList!=null)
            buffer.append(VarDeclExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [SVarDecl]");
        return buffer.toString();
    }
}
