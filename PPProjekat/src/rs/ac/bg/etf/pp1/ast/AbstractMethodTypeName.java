// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class AbstractMethodTypeName implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private XType XType;
    private String abstMethName;

    public AbstractMethodTypeName (XType XType, String abstMethName) {
        this.XType=XType;
        if(XType!=null) XType.setParent(this);
        this.abstMethName=abstMethName;
    }

    public XType getXType() {
        return XType;
    }

    public void setXType(XType XType) {
        this.XType=XType;
    }

    public String getAbstMethName() {
        return abstMethName;
    }

    public void setAbstMethName(String abstMethName) {
        this.abstMethName=abstMethName;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(XType!=null) XType.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(XType!=null) XType.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(XType!=null) XType.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractMethodTypeName(\n");

        if(XType!=null)
            buffer.append(XType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+abstMethName);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractMethodTypeName]");
        return buffer.toString();
    }
}
