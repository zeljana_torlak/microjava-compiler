// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ForStatement extends Statement {

    private ForName ForName;
    private DesignatorStatementOpt DesignatorStatementOpt;
    private CondForStart CondForStart;
    private ConditionOpt ConditionOpt;
    private AfterForStart AfterForStart;
    private DesignatorStatementOpt DesignatorStatementOpt1;
    private ForStart ForStart;
    private Statement Statement;

    public ForStatement (ForName ForName, DesignatorStatementOpt DesignatorStatementOpt, CondForStart CondForStart, ConditionOpt ConditionOpt, AfterForStart AfterForStart, DesignatorStatementOpt DesignatorStatementOpt1, ForStart ForStart, Statement Statement) {
        this.ForName=ForName;
        if(ForName!=null) ForName.setParent(this);
        this.DesignatorStatementOpt=DesignatorStatementOpt;
        if(DesignatorStatementOpt!=null) DesignatorStatementOpt.setParent(this);
        this.CondForStart=CondForStart;
        if(CondForStart!=null) CondForStart.setParent(this);
        this.ConditionOpt=ConditionOpt;
        if(ConditionOpt!=null) ConditionOpt.setParent(this);
        this.AfterForStart=AfterForStart;
        if(AfterForStart!=null) AfterForStart.setParent(this);
        this.DesignatorStatementOpt1=DesignatorStatementOpt1;
        if(DesignatorStatementOpt1!=null) DesignatorStatementOpt1.setParent(this);
        this.ForStart=ForStart;
        if(ForStart!=null) ForStart.setParent(this);
        this.Statement=Statement;
        if(Statement!=null) Statement.setParent(this);
    }

    public ForName getForName() {
        return ForName;
    }

    public void setForName(ForName ForName) {
        this.ForName=ForName;
    }

    public DesignatorStatementOpt getDesignatorStatementOpt() {
        return DesignatorStatementOpt;
    }

    public void setDesignatorStatementOpt(DesignatorStatementOpt DesignatorStatementOpt) {
        this.DesignatorStatementOpt=DesignatorStatementOpt;
    }

    public CondForStart getCondForStart() {
        return CondForStart;
    }

    public void setCondForStart(CondForStart CondForStart) {
        this.CondForStart=CondForStart;
    }

    public ConditionOpt getConditionOpt() {
        return ConditionOpt;
    }

    public void setConditionOpt(ConditionOpt ConditionOpt) {
        this.ConditionOpt=ConditionOpt;
    }

    public AfterForStart getAfterForStart() {
        return AfterForStart;
    }

    public void setAfterForStart(AfterForStart AfterForStart) {
        this.AfterForStart=AfterForStart;
    }

    public DesignatorStatementOpt getDesignatorStatementOpt1() {
        return DesignatorStatementOpt1;
    }

    public void setDesignatorStatementOpt1(DesignatorStatementOpt DesignatorStatementOpt1) {
        this.DesignatorStatementOpt1=DesignatorStatementOpt1;
    }

    public ForStart getForStart() {
        return ForStart;
    }

    public void setForStart(ForStart ForStart) {
        this.ForStart=ForStart;
    }

    public Statement getStatement() {
        return Statement;
    }

    public void setStatement(Statement Statement) {
        this.Statement=Statement;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ForName!=null) ForName.accept(visitor);
        if(DesignatorStatementOpt!=null) DesignatorStatementOpt.accept(visitor);
        if(CondForStart!=null) CondForStart.accept(visitor);
        if(ConditionOpt!=null) ConditionOpt.accept(visitor);
        if(AfterForStart!=null) AfterForStart.accept(visitor);
        if(DesignatorStatementOpt1!=null) DesignatorStatementOpt1.accept(visitor);
        if(ForStart!=null) ForStart.accept(visitor);
        if(Statement!=null) Statement.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ForName!=null) ForName.traverseTopDown(visitor);
        if(DesignatorStatementOpt!=null) DesignatorStatementOpt.traverseTopDown(visitor);
        if(CondForStart!=null) CondForStart.traverseTopDown(visitor);
        if(ConditionOpt!=null) ConditionOpt.traverseTopDown(visitor);
        if(AfterForStart!=null) AfterForStart.traverseTopDown(visitor);
        if(DesignatorStatementOpt1!=null) DesignatorStatementOpt1.traverseTopDown(visitor);
        if(ForStart!=null) ForStart.traverseTopDown(visitor);
        if(Statement!=null) Statement.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ForName!=null) ForName.traverseBottomUp(visitor);
        if(DesignatorStatementOpt!=null) DesignatorStatementOpt.traverseBottomUp(visitor);
        if(CondForStart!=null) CondForStart.traverseBottomUp(visitor);
        if(ConditionOpt!=null) ConditionOpt.traverseBottomUp(visitor);
        if(AfterForStart!=null) AfterForStart.traverseBottomUp(visitor);
        if(DesignatorStatementOpt1!=null) DesignatorStatementOpt1.traverseBottomUp(visitor);
        if(ForStart!=null) ForStart.traverseBottomUp(visitor);
        if(Statement!=null) Statement.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ForStatement(\n");

        if(ForName!=null)
            buffer.append(ForName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorStatementOpt!=null)
            buffer.append(DesignatorStatementOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(CondForStart!=null)
            buffer.append(CondForStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConditionOpt!=null)
            buffer.append(ConditionOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AfterForStart!=null)
            buffer.append(AfterForStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorStatementOpt1!=null)
            buffer.append(DesignatorStatementOpt1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForStart!=null)
            buffer.append(ForStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Statement!=null)
            buffer.append(Statement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ForStatement]");
        return buffer.toString();
    }
}
