// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class DesignFactor extends Factor {

    private Designator Designator;
    private FactorActParsOpt FactorActParsOpt;

    public DesignFactor (Designator Designator, FactorActParsOpt FactorActParsOpt) {
        this.Designator=Designator;
        if(Designator!=null) Designator.setParent(this);
        this.FactorActParsOpt=FactorActParsOpt;
        if(FactorActParsOpt!=null) FactorActParsOpt.setParent(this);
    }

    public Designator getDesignator() {
        return Designator;
    }

    public void setDesignator(Designator Designator) {
        this.Designator=Designator;
    }

    public FactorActParsOpt getFactorActParsOpt() {
        return FactorActParsOpt;
    }

    public void setFactorActParsOpt(FactorActParsOpt FactorActParsOpt) {
        this.FactorActParsOpt=FactorActParsOpt;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Designator!=null) Designator.accept(visitor);
        if(FactorActParsOpt!=null) FactorActParsOpt.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Designator!=null) Designator.traverseTopDown(visitor);
        if(FactorActParsOpt!=null) FactorActParsOpt.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Designator!=null) Designator.traverseBottomUp(visitor);
        if(FactorActParsOpt!=null) FactorActParsOpt.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignFactor(\n");

        if(Designator!=null)
            buffer.append(Designator.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FactorActParsOpt!=null)
            buffer.append(FactorActParsOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignFactor]");
        return buffer.toString();
    }
}
