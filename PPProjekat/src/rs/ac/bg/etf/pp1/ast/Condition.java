// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class Condition implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Struct struct = null;

    private CondTerm CondTerm;
    private CondEnd CondEnd;
    private ConditionExtList ConditionExtList;

    public Condition (CondTerm CondTerm, CondEnd CondEnd, ConditionExtList ConditionExtList) {
        this.CondTerm=CondTerm;
        if(CondTerm!=null) CondTerm.setParent(this);
        this.CondEnd=CondEnd;
        if(CondEnd!=null) CondEnd.setParent(this);
        this.ConditionExtList=ConditionExtList;
        if(ConditionExtList!=null) ConditionExtList.setParent(this);
    }

    public CondTerm getCondTerm() {
        return CondTerm;
    }

    public void setCondTerm(CondTerm CondTerm) {
        this.CondTerm=CondTerm;
    }

    public CondEnd getCondEnd() {
        return CondEnd;
    }

    public void setCondEnd(CondEnd CondEnd) {
        this.CondEnd=CondEnd;
    }

    public ConditionExtList getConditionExtList() {
        return ConditionExtList;
    }

    public void setConditionExtList(ConditionExtList ConditionExtList) {
        this.ConditionExtList=ConditionExtList;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(CondTerm!=null) CondTerm.accept(visitor);
        if(CondEnd!=null) CondEnd.accept(visitor);
        if(ConditionExtList!=null) ConditionExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(CondTerm!=null) CondTerm.traverseTopDown(visitor);
        if(CondEnd!=null) CondEnd.traverseTopDown(visitor);
        if(ConditionExtList!=null) ConditionExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(CondTerm!=null) CondTerm.traverseBottomUp(visitor);
        if(CondEnd!=null) CondEnd.traverseBottomUp(visitor);
        if(ConditionExtList!=null) ConditionExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("Condition(\n");

        if(CondTerm!=null)
            buffer.append(CondTerm.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(CondEnd!=null)
            buffer.append(CondEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConditionExtList!=null)
            buffer.append(ConditionExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [Condition]");
        return buffer.toString();
    }
}
