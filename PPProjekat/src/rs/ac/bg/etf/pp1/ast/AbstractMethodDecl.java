// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class AbstractMethodDecl implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private AbstractMethodTypeName AbstractMethodTypeName;
    private FormParsOpt FormParsOpt;

    public AbstractMethodDecl (AbstractMethodTypeName AbstractMethodTypeName, FormParsOpt FormParsOpt) {
        this.AbstractMethodTypeName=AbstractMethodTypeName;
        if(AbstractMethodTypeName!=null) AbstractMethodTypeName.setParent(this);
        this.FormParsOpt=FormParsOpt;
        if(FormParsOpt!=null) FormParsOpt.setParent(this);
    }

    public AbstractMethodTypeName getAbstractMethodTypeName() {
        return AbstractMethodTypeName;
    }

    public void setAbstractMethodTypeName(AbstractMethodTypeName AbstractMethodTypeName) {
        this.AbstractMethodTypeName=AbstractMethodTypeName;
    }

    public FormParsOpt getFormParsOpt() {
        return FormParsOpt;
    }

    public void setFormParsOpt(FormParsOpt FormParsOpt) {
        this.FormParsOpt=FormParsOpt;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractMethodTypeName!=null) AbstractMethodTypeName.accept(visitor);
        if(FormParsOpt!=null) FormParsOpt.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractMethodTypeName!=null) AbstractMethodTypeName.traverseTopDown(visitor);
        if(FormParsOpt!=null) FormParsOpt.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractMethodTypeName!=null) AbstractMethodTypeName.traverseBottomUp(visitor);
        if(FormParsOpt!=null) FormParsOpt.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractMethodDecl(\n");

        if(AbstractMethodTypeName!=null)
            buffer.append(AbstractMethodTypeName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FormParsOpt!=null)
            buffer.append(FormParsOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractMethodDecl]");
        return buffer.toString();
    }
}
