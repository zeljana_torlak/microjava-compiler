// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDecl implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private AbstractClassName AbstractClassName;
    private ExtendsOpt ExtendsOpt;
    private VarDeclAccessList VarDeclAccessList;
    private XMethodDeclListOpt XMethodDeclListOpt;

    public AbstractClassDecl (AbstractClassName AbstractClassName, ExtendsOpt ExtendsOpt, VarDeclAccessList VarDeclAccessList, XMethodDeclListOpt XMethodDeclListOpt) {
        this.AbstractClassName=AbstractClassName;
        if(AbstractClassName!=null) AbstractClassName.setParent(this);
        this.ExtendsOpt=ExtendsOpt;
        if(ExtendsOpt!=null) ExtendsOpt.setParent(this);
        this.VarDeclAccessList=VarDeclAccessList;
        if(VarDeclAccessList!=null) VarDeclAccessList.setParent(this);
        this.XMethodDeclListOpt=XMethodDeclListOpt;
        if(XMethodDeclListOpt!=null) XMethodDeclListOpt.setParent(this);
    }

    public AbstractClassName getAbstractClassName() {
        return AbstractClassName;
    }

    public void setAbstractClassName(AbstractClassName AbstractClassName) {
        this.AbstractClassName=AbstractClassName;
    }

    public ExtendsOpt getExtendsOpt() {
        return ExtendsOpt;
    }

    public void setExtendsOpt(ExtendsOpt ExtendsOpt) {
        this.ExtendsOpt=ExtendsOpt;
    }

    public VarDeclAccessList getVarDeclAccessList() {
        return VarDeclAccessList;
    }

    public void setVarDeclAccessList(VarDeclAccessList VarDeclAccessList) {
        this.VarDeclAccessList=VarDeclAccessList;
    }

    public XMethodDeclListOpt getXMethodDeclListOpt() {
        return XMethodDeclListOpt;
    }

    public void setXMethodDeclListOpt(XMethodDeclListOpt XMethodDeclListOpt) {
        this.XMethodDeclListOpt=XMethodDeclListOpt;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassName!=null) AbstractClassName.accept(visitor);
        if(ExtendsOpt!=null) ExtendsOpt.accept(visitor);
        if(VarDeclAccessList!=null) VarDeclAccessList.accept(visitor);
        if(XMethodDeclListOpt!=null) XMethodDeclListOpt.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassName!=null) AbstractClassName.traverseTopDown(visitor);
        if(ExtendsOpt!=null) ExtendsOpt.traverseTopDown(visitor);
        if(VarDeclAccessList!=null) VarDeclAccessList.traverseTopDown(visitor);
        if(XMethodDeclListOpt!=null) XMethodDeclListOpt.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassName!=null) AbstractClassName.traverseBottomUp(visitor);
        if(ExtendsOpt!=null) ExtendsOpt.traverseBottomUp(visitor);
        if(VarDeclAccessList!=null) VarDeclAccessList.traverseBottomUp(visitor);
        if(XMethodDeclListOpt!=null) XMethodDeclListOpt.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDecl(\n");

        if(AbstractClassName!=null)
            buffer.append(AbstractClassName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ExtendsOpt!=null)
            buffer.append(ExtendsOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclAccessList!=null)
            buffer.append(VarDeclAccessList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(XMethodDeclListOpt!=null)
            buffer.append(XMethodDeclListOpt.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDecl]");
        return buffer.toString();
    }
}
