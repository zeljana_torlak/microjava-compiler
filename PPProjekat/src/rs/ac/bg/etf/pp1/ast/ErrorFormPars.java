// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ErrorFormPars extends FormPars {

    private FormParsExtList FormParsExtList;

    public ErrorFormPars (FormParsExtList FormParsExtList) {
        this.FormParsExtList=FormParsExtList;
        if(FormParsExtList!=null) FormParsExtList.setParent(this);
    }

    public FormParsExtList getFormParsExtList() {
        return FormParsExtList;
    }

    public void setFormParsExtList(FormParsExtList FormParsExtList) {
        this.FormParsExtList=FormParsExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(FormParsExtList!=null) FormParsExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(FormParsExtList!=null) FormParsExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(FormParsExtList!=null) FormParsExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ErrorFormPars(\n");

        if(FormParsExtList!=null)
            buffer.append(FormParsExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ErrorFormPars]");
        return buffer.toString();
    }
}
