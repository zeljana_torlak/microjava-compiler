// generated with ast extension for cup
// version 0.8
// 4/5/2020 21:29:42


package rs.ac.bg.etf.pp1.ast;

public class ErrorVarDeclExtList extends VarDeclExtList {

    private VarDeclExtList VarDeclExtList;

    public ErrorVarDeclExtList (VarDeclExtList VarDeclExtList) {
        this.VarDeclExtList=VarDeclExtList;
        if(VarDeclExtList!=null) VarDeclExtList.setParent(this);
    }

    public VarDeclExtList getVarDeclExtList() {
        return VarDeclExtList;
    }

    public void setVarDeclExtList(VarDeclExtList VarDeclExtList) {
        this.VarDeclExtList=VarDeclExtList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclExtList!=null) VarDeclExtList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclExtList!=null) VarDeclExtList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclExtList!=null) VarDeclExtList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ErrorVarDeclExtList(\n");

        if(VarDeclExtList!=null)
            buffer.append(VarDeclExtList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ErrorVarDeclExtList]");
        return buffer.toString();
    }
}
