package rs.ac.bg.etf.pp1;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.CounterVisitor.*;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class CodeGenerator extends VisitorAdaptor {
	
	LinkedList<Integer> nextOrAdr = new LinkedList<Integer>();
	LinkedList<Integer> trueConditionAdr = new LinkedList<Integer>();
	LinkedList<LinkedList<Integer>> falseConditionAdr = new LinkedList<LinkedList<Integer>>();
	LinkedList<Integer> elseBegins = new LinkedList<Integer>();
	LinkedList<LinkedList<Integer>> breakAdr = new LinkedList<LinkedList<Integer>>();
	
	LinkedList<Integer> forConditionBegins = new LinkedList<Integer>();
	LinkedList<Integer> forAfterBegins = new LinkedList<Integer>();
	
	LinkedList<Character> nextOpLeft = new LinkedList<Character>();
	LinkedList<Character> nextOpRight = new LinkedList<Character>();
	
	LinkedList<LinkedList<Obj>> opRightObj = new LinkedList<LinkedList<Obj>>();
	
	LinkedList<Boolean> designatorStore = new LinkedList<Boolean>();
	
	VirtualMethodTable vmt = new VirtualMethodTable();
	
	private int mainPc;
	
	public int getMainPc(){
		return mainPc;
	}
	
	public CodeGenerator() {
		Tab.chrObj.setAdr(Code.pc);
		Tab.ordObj.setAdr(Code.pc);
		createEmbeddedFunction(false);
		Tab.lenObj.setAdr(Code.pc);
		createEmbeddedFunction(true);
	}
	
	private void createEmbeddedFunction(boolean isLen) {
		Code.put(Code.enter);
		Code.put(1);
		Code.put(1);
		Code.put(Code.load_n + 0);
		if (isLen) Code.put(Code.arraylength);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}	
	
	public void visit(MethodTypeName methodTypeName) {
		methodTypeName.obj.setAdr(Code.pc);
		if ((methodTypeName.getMethName()).equals("main")) {
			mainPc = Code.pc;
		}
		
		SyntaxNode parent = methodTypeName.getParent();
		VarCounter varCnt = new VarCounter();
		parent.traverseBottomUp(varCnt); //method.getLevel()
		FormParCounter formParCnt = new FormParCounter();
		parent.traverseBottomUp(formParCnt); //method.getLocalSymbols().size()
		int thisCnt = 0;
		if (methodTypeName.obj.getFpPos() > 0) thisCnt = 1;
		
		methodTypeName.obj.setLevel(formParCnt.getCount()+thisCnt);
		
		Code.put(Code.enter);
		Code.put(formParCnt.getCount()+thisCnt);
		Code.put(formParCnt.getCount() + varCnt.getCount()+thisCnt);
		
		if ((methodTypeName.getMethName()).equals("main")) {
			vmt.insertTable();
			LinkedList<Obj> list = new LinkedList<Obj>(Tab.currentScope().values());
			for (Obj elem: list) {
				if (elem.getKind() == Obj.Prog) { //samo za 1 program
					list = new LinkedList<Obj>(elem.getLocalSymbols());
					break; 
				}
			}
			for (Obj elem: list) {
				if (elem.getKind() == Obj.Var && elem.getLevel() == 0)
					elem.setAdr(elem.getAdr() + vmt.getSize());
			}
		}
	}
	
	public void visit(MethodDecl methodDecl) {
		if (methodDecl.getMethodTypeName().getXType().struct == Tab.noType) {
			Code.put(Code.exit);
			Code.put(Code.return_);
		} else { 
			Code.put(Code.trap);
			Code.put(1);
		}
	}
	
	public void visit(ReturnStatement returnStatement) {
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void visit(WithSubOpt withSubOpt) {
		Code.put(Code.neg);
	}
	
	public void visit(AddAddopLeft addAddopLeft) {
		nextOpLeft.add('+');
	}
	
	public void visit(SubAddopLeft subAddopLeft) {
		nextOpLeft.add('-');
	}
	
	public void visit(MulMulopLeft mulMulopLeft) {
		nextOpLeft.add('*');
	}
	
	public void visit(DivMulopLeft divMulopLeft) {
		nextOpLeft.add('/');
	}
	
	public void visit(ModMulopLeft modMulopLeft) {
		nextOpLeft.add('%');
	}
	
	public void visit(AddassignAddopRight addassignAddopRight) {
		nextOpRight.add('+');
	}
	
	public void visit(SubassignAddopRight subassignAddopRight) {
		nextOpRight.add('-');
	}
	
	public void visit(MulassignMulopRight mulassignMulopRight) {
		nextOpRight.add('*');
	}
	
	public void visit(DivassignMulopRight divassignMulopRight) {
		nextOpRight.add('/');
	}
	
	public void visit(ModassignMulopRight modassignMulopRight) {
		nextOpRight.add('%');
	}
	
	public void visit(ExprEnd exprEnd) {
		ExprExtListS parent = (ExprExtListS) exprEnd.getParent();
		if (parent.getAddop() instanceof AddopAddopLeft) {
			char op = nextOpLeft.removeLast();
			if (op == '+') Code.put(Code.add);
			else if (op == '-') Code.put(Code.sub);
		}
	}
	
	public void visit(TermEnd termEnd) {
		TermExtListS parent = (TermExtListS) termEnd.getParent();
		if (parent.getMulop() instanceof MulopMulopLeft) {
			char op = nextOpLeft.removeLast();
			if (op == '*') Code.put(Code.mul);
			else if (op == '/') Code.put(Code.div);
			else if (op == '%') Code.put(Code.rem);
		}			
	}
	
	public void visit(ExprStart exprStart) {
		opRightObj.add(new LinkedList<Obj>());
	}
	
	public void visit(Expr expr) {
		LinkedList<Obj> objs = opRightObj.removeLast();
		
		while (objs.size() != 0) {
			Obj temp = objs.removeLast();
			
			char op = nextOpRight.removeLast();
			if (op == '+') Code.put(Code.add);
			else if (op == '-') Code.put(Code.sub);
			else if (op == '*') Code.put(Code.mul);
			else if (op == '/') Code.put(Code.div);
			else if (op == '%') Code.put(Code.rem);
			if (temp.getKind() == Obj.Elem) Code.put(Code.dup_x2);
			else if (temp.getKind() == Obj.Fld) Code.put(Code.dup_x1);
			else Code.put(Code.dup);
			Code.store(temp);//
		}
	}
	
	public void visit(WithCondFactExtOpt withCondFactExtOpt) {
		if (withCondFactExtOpt.getRelop() instanceof EqRelop)
			Code.putFalseJump(Code.eq, 0);
		else if (withCondFactExtOpt.getRelop() instanceof NeqRelop)
			Code.putFalseJump(Code.ne, 0);
		else if (withCondFactExtOpt.getRelop() instanceof GtRelop)
			Code.putFalseJump(Code.gt, 0);
		else if (withCondFactExtOpt.getRelop() instanceof GeRelop)
			Code.putFalseJump(Code.ge, 0);
		else if (withCondFactExtOpt.getRelop() instanceof LtRelop)
			Code.putFalseJump(Code.lt, 0);
		else if (withCondFactExtOpt.getRelop() instanceof LeRelop)
			Code.putFalseJump(Code.le, 0);
		nextOrAdr.add(Code.pc-2);
	}
	
	public void visit(NoCondFactExtOpt noCondFactExtOpt) {
		Code.loadConst(0);
		Code.putFalseJump(Code.ne, 0);
		nextOrAdr.add(Code.pc-2);
	}
	
	public void visit(CondEnd condEnd) {
		boolean last = false;
		if (condEnd.getParent().getClass() == Condition.class) {
			Condition parent = (Condition) condEnd.getParent();
			if (parent.getConditionExtList() instanceof NoConditionExtList) 
				last = true;
		} else if (condEnd.getParent().getClass() == ConditionExtListS.class) {
			ConditionExtListS parent = (ConditionExtListS) condEnd.getParent();
			if (parent.getConditionExtList() instanceof NoConditionExtList) 
				last = true;
		}
		
		if (last) {
			falseConditionAdr.add(new LinkedList<Integer>());
			for (Integer adr: nextOrAdr)
				falseConditionAdr.getLast().add(adr);
		} else {		
			for (Integer adr: nextOrAdr)
				Code.fixup(adr);
		}
		nextOrAdr.clear();
	}
	
	public void visit(NoCondTermExtList noCondTermExtList) {
		Code.putJump(0);
		trueConditionAdr.add(Code.pc-2);
	}
	
	public void visit(NoConditionExtList NoConditionExtList) {
		for (Integer adr: trueConditionAdr) {
			Code.fixup(adr);
		}
		trueConditionAdr.clear();
	}
	
	public void visit(ElseStart elseStart) {
		Code.putJump(0);
		elseBegins.add(Code.pc);
		
		for (Integer adr: falseConditionAdr.getLast()) {
			Code.fixup(adr);
		}
		falseConditionAdr.removeLast();
	}
	
	public void visit(ForeachName foreachName) {
		Designator designator = ((ForeachStatement) foreachName.getParent()).getDesignator();
		Code.loadConst(0);
		
		forConditionBegins.add(Code.pc);
		breakAdr.add(new LinkedList<Integer>());
		//condition
		Code.put(Code.dup);
		(designator).traverseBottomUp(this);
		if (designator.obj.getKind() != Obj.Fld) Code.load(designator.obj); //
		Code.put(Code.arraylength);
		Code.putFalseJump(Code.lt, 0);
		breakAdr.getLast().add(Code.pc-2);
		
		Code.putJump(0);
		forAfterBegins.add(Code.pc);
		//after
		Code.put(Code.const_1);
		Code.put(Code.add);
		
		Code.putJump(forConditionBegins.getLast());
	}
	
	public void visit(CondForStart condForStart) {
		forConditionBegins.add(Code.pc);
		breakAdr.add(new LinkedList<Integer>());
	}
	
	public void visit(AfterForStart afterForStart) {
		Code.putJump(0);
		forAfterBegins.add(Code.pc);
	}
	
	public void visit(ForStart forStart) {
		if (forStart.getParent().getClass() != ForeachStatement.class) 
			Code.putJump(forConditionBegins.getLast());
		Code.fixup(forAfterBegins.getLast()-2);
		
		if (forStart.getParent().getClass() == ForeachStatement.class) {
			Designator designator = ((ForeachStatement) forStart.getParent()).getDesignator();
			Obj var = ((ForeachStatement) forStart.getParent()).getForeachName().obj;
			
			Code.put(Code.dup);
			Code.store(var); //new Obj(Obj.Var, var.getName(), Tab.intType, var.getAdr(), var.getLevel())
			
			(designator).traverseBottomUp(this);
			if (designator.obj.getKind() != Obj.Fld) Code.load(designator.obj); //

			Code.load(var);
			Code.put(Code.aload);			
			Code.store(var);
		}
	}
	
	public void visit(IfElseStatement ifElseStatement) {
		Code.fixup(elseBegins.removeLast()-2);
	}
	
	public void visit(IfStatement ifStatement) {
		for (Integer adr: falseConditionAdr.getLast()) {
			Code.fixup(adr);
		}
		falseConditionAdr.removeLast();
	}
	
	public void visit(ForStatement forStatement) {
		Code.putJump(forAfterBegins.getLast());
		
		if (!(forStatement.getConditionOpt() instanceof NoConditionOpt)) { 
			for (Integer adr: falseConditionAdr.getLast())
				Code.fixup(adr);

			falseConditionAdr.removeLast();
		}
		
		for (Integer adr: breakAdr.getLast()) {
			Code.fixup(adr);
		}
		breakAdr.removeLast();
		
		forConditionBegins.removeLast();
		forAfterBegins.removeLast();
	}
	
	public void visit(ForeachStatement foreachStatement) {
		Code.putJump(forAfterBegins.getLast());
		
		for (Integer adr: breakAdr.getLast()) {
			Code.fixup(adr);
		}
		breakAdr.removeLast();
		
		forConditionBegins.removeLast();
		forAfterBegins.removeLast();
		
		Code.put(Code.pop);
	}
	
	public void visit(BreakStatement breakStatement) {
		Code.putJump(0);
		breakAdr.getLast().add(Code.pc-2);
	}
	
	public void visit(ContinueStatement continueStatement) {
		Code.putJump(forAfterBegins.getLast());
	}
	
	public void visit(ReadStatement readStatement) {
		Obj temp = readStatement.getDesignator().obj;
		if (temp.getType() != Tab.charType)
			Code.put(Code.read);
		else 
			Code.put(Code.bread);
		Code.store(temp);
	}
	
	public void visit(PrintStatement printStatement) {
		if (printStatement.getExpr().obj.getType() == Tab.charType) //dodati bool
			Code.put(Code.bprint);
		else 
			Code.put(Code.print);
	}
	
	public void visit(WithExprNumConstOpt withExprNumConstOpt) {
		Code.loadConst(withExprNumConstOpt.getN1());
	}
	
	public void visit(NoExprNumConstOpt noExprNumConstOpt) {
		PrintStatement parent = (PrintStatement) noExprNumConstOpt.getParent();
		if (parent.getExpr().obj.getType() == Tab.intType)
			Code.loadConst(5);
		else //dodati bool
			Code.loadConst(1);
	}
	
	public void visit(AssignopXDesignatorStatementExt assignopXDesignatorStatementExt) {
		DesignatorStatement parent = (DesignatorStatement) assignopXDesignatorStatementExt.getParent();
		Obj temp = parent.getDesignator().obj;
		if (assignopXDesignatorStatementExt.getAssignop() instanceof AddopRightAssignop || assignopXDesignatorStatementExt.getAssignop() instanceof MulopRightAssignop) {
			char op = nextOpRight.removeLast();
			if (op == '+') Code.put(Code.add);
			else if (op == '-') Code.put(Code.sub);
			else if (op == '*') Code.put(Code.mul);
			else if (op == '/') Code.put(Code.div);
			else if (op == '%') Code.put(Code.rem);
			if (temp.getKind() == Obj.Elem) Code.put(Code.dup_x2);
			else if (temp.getKind() == Obj.Fld) Code.put(Code.dup_x1);
			else Code.put(Code.dup);
			Code.store(temp);
			Code.put(Code.pop);
		} else if (assignopXDesignatorStatementExt.getAssignop() instanceof AssignAssignop)
			Code.store(temp);//
	}
	
	public void visit(IncrXDesignatorStatementExt incrXDesignatorStatementExt) {
		DesignatorStatement parent = (DesignatorStatement) incrXDesignatorStatementExt.getParent();
		Obj temp = parent.getDesignator().obj;
		if (parent.getDesignator().obj.getKind() == Obj.Elem)
			Code.put(Code.dup2);
		else if (parent.getDesignator().obj.getKind() == Obj.Fld)
			Code.put(Code.dup);
		Code.load(temp);
		Code.put(Code.const_1);
		Code.put(Code.add); //inc
		Code.store(temp);
	}
	
	public void visit(DecrXDesignatorStatementExt decrXDesignatorStatementExt) {
		DesignatorStatement parent = (DesignatorStatement) decrXDesignatorStatementExt.getParent();
		Obj temp = parent.getDesignator().obj;
		if (parent.getDesignator().obj.getKind() == Obj.Elem)
			Code.put(Code.dup2);
		else if (parent.getDesignator().obj.getKind() == Obj.Fld)
			Code.put(Code.dup);
		Code.load(temp);
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.store(temp);
	}

	public void visit(ActParsXDesignatorStatementExt actParsXDesignatorStatementExt) {
		Designator designator = ((DesignatorStatement) actParsXDesignatorStatementExt.getParent()).getDesignator();
		
		callProcessing(designator);
		
		if (designator.obj.getType() != Tab.noType)
			Code.put(Code.pop);
	}
	
	public void visit(WithFactorActParsOpt withFactorActParsOpt) {
		Designator designator = ((DesignFactor) withFactorActParsOpt.getParent()).getDesignator();
		
		callProcessing(designator);
	}
	
	private void callProcessing(Designator designator) {
		Obj method = designator.obj;
		
		if (method.getFpPos() > 0) {
			(designator).traverseBottomUp(this);
			
			Code.put(Code.getfield);
			Code.put2(0);
			Code.put(Code.invokevirtual);
			String methodName = method.getName();
			for (int i = 0; i < methodName.length(); i++) {
				Code.put4(methodName.charAt(i));
			}
			Code.put4(-1);
		} else {
			int offset = method.getAdr() - Code.pc;
			Code.put(Code.call);
			Code.put2(offset);
		}
	}
	
	public void visit(NumConstFactor numConstFactor) {
		Obj temp = Tab.insert(Obj.Con, "$", numConstFactor.obj.getType());
		temp.setLevel(0);
		temp.setAdr(numConstFactor.getN1());
		Code.load(temp);
	}
	
	public void visit(CharConstFactor charConstFactor) {
		Obj temp = Tab.insert(Obj.Con, "$", charConstFactor.obj.getType());
		temp.setLevel(0);
		temp.setAdr(charConstFactor.getC1());
		Code.load(temp);
	}
	
	public void visit(TrueConstFactor trueConstFactor) {
		Obj temp = Tab.insert(Obj.Con, "$", trueConstFactor.obj.getType());
		temp.setLevel(0);
		temp.setAdr(1);
		Code.load(temp);
	}
	
	public void visit(FalseConstFactor falseConstFactor) {
		Obj temp = Tab.insert(Obj.Con, "$", falseConstFactor.obj.getType());
		temp.setLevel(0);
		temp.setAdr(0);
		Code.load(temp);
	}
	
	/*
	Integer hlpAdr;
	Integer afterBegin;
	
	public void visit(SpecFactor specFactor) {
		Designator designator = specFactor.getDesignator();
		
		Code.fixup(afterBegin-2);
		Code.put(Code.dup_x1);
		
		(designator).traverseBottomUp(this);
		if (designator.obj.getKind() != Obj.Fld) Code.load(designator.obj);
		
		Code.put(Code.dup2);
		Code.put(Code.pop);
		Code.put(Code.aload);
		
		Code.put(Code.dup_x1);
		Code.put(Code.pop);
		Code.put(Code.pop);
		
		Code.put(Code.dup2);
		Code.putFalseJump(Code.ge, 0); //max
		int tmp1 = Code.pc-2;
		Code.put(Code.pop);
		Code.put(Code.dup_x1);
		Code.put(Code.pop);

		Code.putJump(0);
		int tmp2 = Code.pc-2;
		Code.fixup(tmp1);
		
		Code.put(Code.dup_x1);
		Code.put(Code.pop);
		Code.put(Code.pop);
		Code.put(Code.dup_x1);
		Code.put(Code.pop);
		
		Code.fixup(tmp2);
		Code.putJump(afterBegin);
		
		Code.fixup(hlpAdr);
		hlpAdr = null;
		afterBegin = null;
		Code.put(Code.pop);
	}
	
	public void visit(SpecName specName) {
		Designator designator = ((SpecFactor) specName.getParent()).getDesignator();
		
		//ako je prazan niz onda trap
		Code.loadConst(0);
		(designator).traverseBottomUp(this);
		if (designator.obj.getKind() != Obj.Fld) Code.load(designator.obj); //
		Code.put(Code.arraylength);
		Code.putFalseJump(Code.eq, 0);
		int tmp1 = Code.pc-2;
		Code.put(Code.trap);
		Code.put(1);
		
		Code.fixup(tmp1);
		//na pocetku je nulti max
		(designator).traverseBottomUp(this);
		if (designator.obj.getKind() != Obj.Fld) Code.load(designator.obj); //
		Code.loadConst(0);
		Code.put(Code.aload);
		
		//else
		Code.loadConst(1);
		
		int loopBegin = Code.pc;
		//condition
		Code.put(Code.dup);
		(designator).traverseBottomUp(this);
		if (designator.obj.getKind() != Obj.Fld) Code.load(designator.obj); //
		Code.put(Code.arraylength);
		Code.putFalseJump(Code.lt, 0);
		hlpAdr = Code.pc-2;
		
		Code.putJump(0);
		afterBegin = Code.pc;
		//after
		Code.put(Code.const_1);
		Code.put(Code.add);
		
		Code.putJump(loopBegin);
		
	}
	*/
	
	public void visit(WithFactorExprExtOpt withFactorExprExtOpt) {
		Code.put(Code.newarray);
		if (withFactorExprExtOpt.struct.getElemType() == Tab.charType)
			Code.put(0);
		else 
			Code.put(1);
	}
	
	public void visit(NoFactorExprExtOpt noFactorExprExtOpt) {
		NewFactor parent = (NewFactor) noFactorExprExtOpt.getParent();
		Code.put(Code.new_);
		Code.put2(parent.getType().struct.getNumberOfFields()*4); //*4
		
		Code.put(Code.dup);
		Code.loadConst(vmt.getVftpAddrForClass(parent.getType().getTypeName()));
		Code.put(Code.putfield);
		Code.put2(0);
	}
	
	public void visit (DesignatorStart designatorStart) {
		Designator designator = (Designator) designatorStart.getParent();
		SyntaxNode parent = designator.getParent();
		designatorStore.add(false);
		
		if (designatorStart.obj != Tab.noObj && designator.getDesignatorEnd().obj.getKind() != Obj.Con)
			Code.load(designatorStart.obj);
		
		if (parent.getClass() == DesignatorStatement.class || parent.getClass() == ReadStatement.class) {
			designatorStore.set(designatorStore.size()-1, true);
			
		} else if (parent.getClass() == DesignFactor.class) {
			if (parent.getParent().getClass() == Term.class) {
				Term grandParent = (Term) parent.getParent();
				if (grandParent.getTermExtList() instanceof TermExtListS) {
					TermExtListS hlp = (TermExtListS) grandParent.getTermExtList();
					if (hlp.getMulop() instanceof MulopMulopRight)
						designatorStore.set(designatorStore.size()-1, true);
				}
				
				if (grandParent.getParent().getClass() == WithSubOpt.class || grandParent.getParent().getClass() == NoSubOpt.class) {
					Expr great = (Expr) grandParent.getParent().getParent();
					if (great.getExprExtList() instanceof ExprExtListS) {
						ExprExtListS hlp = (ExprExtListS) great.getExprExtList();
						if (hlp.getAddop() instanceof AddopAddopRight)
							designatorStore.set(designatorStore.size()-1, true);
					}
				} else if (grandParent.getParent().getClass() == ExprExtListS.class) {
					ExprExtListS great = (ExprExtListS) grandParent.getParent();
					if (great.getExprExtList() instanceof ExprExtListS) {
						ExprExtListS hlp = (ExprExtListS) great.getExprExtList();
						if (hlp.getAddop() instanceof AddopAddopRight)
							designatorStore.set(designatorStore.size()-1, true);
					}
				}
				
			} else if (parent.getParent().getClass() == TermExtListS.class) {
				TermExtListS grandParent = (TermExtListS) parent.getParent();
				if (grandParent.getTermExtList() instanceof TermExtListS) {
					TermExtListS hlp = (TermExtListS) grandParent.getTermExtList();
					if (hlp.getMulop() instanceof MulopMulopRight)
						designatorStore.set(designatorStore.size()-1, true);
				}
			}
		}
	}
	
	public void visit(DesignatorEnd designatorEnd) {
		if (designatorEnd.getParent().getClass() == Designator.class) {
			Designator parent = (Designator) designatorEnd.getParent();
			if (!(parent.getXDesignatorExtList() instanceof NoXDesignatorExtList)) {
				if (designatorEnd.obj.getKind() != Obj.Meth) 
					Code.load(designatorEnd.obj);
			}
			
		} else if (designatorEnd.getParent().getClass() == DotXDesignatorExtList.class) {
			DotXDesignatorExtList parent = (DotXDesignatorExtList) designatorEnd.getParent();
			
			Obj temp = null;
			if (parent.getParent().getClass() == Designator.class)
				temp = ((Designator) parent.getParent()).getDesignatorEnd().obj;
			else if (parent.getParent().getClass() == DotXDesignatorExtList.class)
				temp = ((DotXDesignatorExtList) parent.getParent()).getDesignatorEnd().obj;
			else if (parent.getParent().getClass() == SqrBraceXDesignatorExtList.class)
				temp = ((SqrBraceXDesignatorExtList) parent.getParent()).getDesignatorEnd().obj;			
			if (temp.getKind() == Obj.Elem)
				Code.load(temp);

			if (!(parent.getXDesignatorExtList() instanceof NoXDesignatorExtList))
				if (designatorEnd.obj.getKind() != Obj.Meth) 
					Code.load(designatorEnd.obj);
			
		} else if (designatorEnd.getParent().getClass() == SqrBraceXDesignatorExtList.class) {
		}
	}
	
	public void visit(Designator designator) {//parent: ForeachStatement, ReadStatement, DesignatorStatement, DesignFactor
		if (designator.obj.getKind() == Obj.Elem || designator.obj.getKind() == Obj.Fld) {			
			if (!designatorStore.getLast()) 
				Code.load(designator.obj);
		} else if (designator.getParent().getClass() == DesignFactor.class && designator.obj.getKind() != Obj.Meth) {
			Code.load(designator.obj);
		}
		
		designatorStore.removeLast();
	}
	
	public void visit(AddopRightAssignop addopRightAssignop) {
		DesignatorStatement grandParent = (DesignatorStatement) addopRightAssignop.getParent().getParent();
		Obj temp = grandParent.getDesignator().obj;
		if (temp.getKind() == Obj.Elem) Code.put(Code.dup2);
		else if (temp.getKind() == Obj.Fld) Code.put(Code.dup);
		Code.load(temp);
	}
	
	public void visit(MulopRightAssignop mulopRightAssignop) {
		DesignatorStatement grandParent = (DesignatorStatement) mulopRightAssignop.getParent().getParent();
		Obj temp = grandParent.getDesignator().obj;
		if (temp.getKind() == Obj.Elem) Code.put(Code.dup2);
		else if (temp.getKind() == Obj.Fld) Code.put(Code.dup);
		Code.load(temp);
	}

	public void visit(AddopAddopRight addopAddopRight) {
		ExprExtListS parent = (ExprExtListS) addopAddopRight.getParent();
		Obj temp = null;
		if (parent.getParent().getClass() == Expr.class)
			temp = ((Expr) parent.getParent()).getSubOpt().obj;
		else if (parent.getParent().getClass() == ExprExtListS.class)
			temp = ((ExprExtListS) parent.getParent()).getTerm().obj;
		if (temp.getKind() == Obj.Elem) {
			Code.put(Code.dup2);
			Code.load(temp);
		} else if (temp.getKind() == Obj.Fld) {
			Code.put(Code.dup);
			Code.load(temp);
		}
		opRightObj.getLast().add(temp);
	}
	
	public void visit(MulopMulopRight mulopMulopRight) {
		TermExtListS parent = (TermExtListS) mulopMulopRight.getParent();
		Obj temp = null;
		if (parent.getParent().getClass() == Term.class)
			temp = ((Term) parent.getParent()).getFactor().obj;
		else if (parent.getParent().getClass() == TermExtListS.class)
			temp = ((TermExtListS) parent.getParent()).getFactor().obj;
		if (temp.getKind() == Obj.Elem) {
			Code.put(Code.dup2);
			Code.load(temp);
		} else if (temp.getKind() == Obj.Fld) {
			Code.put(Code.dup);
			Code.load(temp);
		}
		opRightObj.getLast().add(temp);
	}
	
	public void visit(WithExtendsOpt withExtendsOpt) {
		if (withExtendsOpt.getParent().getClass() != SClassDecl.class) return;
		LinkedList<Obj> fields = new LinkedList<Obj>(((SClassDecl) withExtendsOpt.getParent()).obj.getType().getMembers());
		
		LinkedList<Obj> inherited = new LinkedList<Obj>(withExtendsOpt.getType().struct.getMembers());
		for (Obj i: inherited) {
			for (Obj f: fields) {
				if ((i.getName()).equals(f.getName())) {
					f.setAdr(i.getAdr());
					break;
				}
			}
		}
	}
	
	public void visit(ClassName className) {
		vmt.classRegistration(className.getClassName());
	}
	
	public void visit(SClassDecl sClassDecl) {
		LinkedList<Obj> objNodes = new LinkedList<Obj>(sClassDecl.obj.getType().getMembers());
		for (Obj obj: objNodes) {
			if (obj.getKind() == Obj.Meth) {
				vmt.addFunctionEntry(obj.getName(), obj.getAdr());
			}
		}
		vmt.addTableTerminator();
	}
	
}

	
