package rs.ac.bg.etf.pp1;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;
import rs.etf.pp1.symboltable.factory.SymbolTableFactory;
import rs.etf.pp1.symboltable.structure.SymbolDataStructure;

public class SemanticAnalyzer extends VisitorAdaptor {
	
	Struct varType = null, constType = null;
	int accessRight = 0;
	LinkedList<Obj> currentDesignatorList = new LinkedList<Obj>();
	
	Obj overridenObj = null;
	Obj currentMethod = null;
	LinkedList<Obj> currentCalls = new LinkedList<Obj>();
	LinkedList<LinkedList<Struct>> actParsPerMeth = new LinkedList<LinkedList<Struct>>();
	
	LinkedList<Obj> parentClasses = new LinkedList<Obj>();
	
	Obj currentClass = null;
	
	boolean mainExists = false, returnFound = false;
	int forLevelCounter = 0;

	boolean errorDetected = false;
	int nVars = 0;
	
	enum TypeOfCheck { EQUALS, COMPATIBLE_WITH, ASSIGNABLE_TO };
	
	Logger log = Logger.getLogger(getClass());
	
	public static final Struct boolType = new Struct(Struct.Bool);
	
	public SemanticAnalyzer() {
		Tab.insert(Obj.Type, "bool", boolType);
	}
    
	public void report_error(String message, SyntaxNode info) {
		errorDetected = true;
		StringBuilder msg = new StringBuilder("Semantic error");
		int line = (info == null) ? 0: info.getLine();
		if (line != 0) msg.append(" on line ").append(line);
		msg.append(": ").append(message);
		log.error(msg.toString());
	}

	public void report_info(String message, SyntaxNode info, Obj obj) {
		StringBuilder msg = new StringBuilder(message); 
		int line = (info == null) ? 0: info.getLine();
		if (line != 0) msg.append(" on line ").append(line);
		if (obj != null) {
			MyDumpSymbolTableVisitor stv = new MyDumpSymbolTableVisitor();
			stv.visitObjNode(obj);
			msg.append(": " + stv.getOutput());
		}
		log.info(msg.toString());
	}
	
	public void visit(ProgName progName) {
		progName.obj = Tab.insert(Obj.Prog, progName.getProgName(), Tab.noType);
		Tab.openScope();
	}
	
	public void visit(Program program) {
		nVars = Tab.currentScope.getnVars();
		Tab.chainLocalSymbols(program.getProgName().obj);
		Tab.closeScope();
		
		if (!mainExists)
			report_error("Main function must be defined! ", program);
	}
	
	public void visit(TypeName typeName) {
		if (typeName.getParent().getClass() == ConstDecl.class)
			constType = typeName.getType().struct;
		else if (typeName.getParent().getClass() == SVarDecl.class) {
			varType = typeName.getType().struct;
			SyntaxNode grandParent = ((SVarDecl) typeName.getParent()).getParent();
			if (grandParent.getClass() == PrivateVarDeclAccessListS.class) accessRight = 1;
			else if (grandParent.getClass() == ProtectedVarDeclAccessListS.class) accessRight = 2;
			else if (grandParent.getClass() == PublicVarDeclAccessListS.class) accessRight = 3;
			else accessRight = 0;
		}
	}
	
	public void visit(ConstDecl constDecl) {
		constType = null;
	}
	
	public void visit(NumXConst numXConst) {
		String constName = null;
		if (numXConst.getParent().getClass() == ConstDecl.class)
			constName = ((ConstDecl) numXConst.getParent()).getConstName();
		else if (numXConst.getParent().getClass() == ConstDeclExtListS.class)
			constName = ((ConstDeclExtListS) numXConst.getParent()).getConstName();
		
		Obj temp;
		if (constType == Tab.intType) {
			(temp = Tab.insert(Obj.Con, constName, constType)).setAdr(numXConst.getN1());
			report_info("Symbolic constant defined " + constName, numXConst.getParent(), temp);
		} else
			report_error("Wrong type of constant " + constName + "! ", numXConst);
	}

	public void visit(CharXConst charXConst) {
		String constName = null;
		if (charXConst.getParent().getClass() == ConstDecl.class)
			constName = ((ConstDecl) charXConst.getParent()).getConstName();
		else if (charXConst.getParent().getClass() == ConstDeclExtListS.class)
			constName = ((ConstDeclExtListS) charXConst.getParent()).getConstName();
		
		Obj temp;
		if (constType == Tab.charType) {
			(temp = Tab.insert(Obj.Con, constName, constType)).setAdr(charXConst.getC1());
			report_info("Symbolic constant defined " + constName, charXConst.getParent(), temp);
		} else
			report_error("Wrong type of constant " + constName + "! ", charXConst);
	}

	public void visit(TrueXConst trueXConst) {
		String constName = null;
		if (trueXConst.getParent().getClass() == ConstDecl.class)
			constName = ((ConstDecl) trueXConst.getParent()).getConstName();
		else if (trueXConst.getParent().getClass() == ConstDeclExtListS.class)
			constName = ((ConstDeclExtListS) trueXConst.getParent()).getConstName();
		
		Obj temp;
		if (constType == boolType) {
			(temp = Tab.insert(Obj.Con, constName, constType)).setAdr(1);
			report_info("Symbolic constant defined " + constName, trueXConst.getParent(), temp);
		} else
			report_error("Wrong type of constant " + constName + "! ", trueXConst);
	}

	public void visit(FalseXConst falseXConst) {
		String constName = null;
		if (falseXConst.getParent().getClass() == ConstDecl.class)
			constName = ((ConstDecl) falseXConst.getParent()).getConstName();
		else if (falseXConst.getParent().getClass() == ConstDeclExtListS.class)
			constName = ((ConstDeclExtListS) falseXConst.getParent()).getConstName();
		
		Obj temp;
		if (constType == boolType) {
			(temp = Tab.insert(Obj.Con, constName, constType)).setAdr(0);
			report_info("Symbolic constant defined " + constName, falseXConst.getParent(), temp);
		} else
			report_error("Wrong type of constant " + constName + "! ", falseXConst);
	}
	
	public void visit(SVarDecl sVarDecl){
		varType = null;
		accessRight = 0;
	}
	
	public void visit(WithSqrBraceOpt withSqrBraceOpt) {
		String varName = null;
		boolean okVar = true;
		if (withSqrBraceOpt.getParent().getClass() == SVarDecl.class) 
			varName = ((SVarDecl) withSqrBraceOpt.getParent()).getVarName();
		else if (withSqrBraceOpt.getParent().getClass() == VarDeclExtListS.class)
			varName = ((VarDeclExtListS) withSqrBraceOpt.getParent()).getVarName();
		else okVar = false;
		if (okVar) {
			Obj temp;
			if (currentClass != null && currentMethod == null) temp = Tab.insert(Obj.Fld, varName, new Struct(Struct.Array, varType));
			else temp = Tab.insert(Obj.Var, varName, new Struct(Struct.Array, varType));
			if (currentClass != null) 
				temp.setFpPos(accessRight);
			report_info("Variable " + varName + " declared", withSqrBraceOpt.getParent(), temp);
		}

		String formParmName = null;
		boolean okFormParm = true;
		Struct formParmType = null;
		if (withSqrBraceOpt.getParent().getClass() == SFormPars.class) {
			formParmName =  ((SFormPars) withSqrBraceOpt.getParent()).getFormParmName();
			formParmType = ((SFormPars) withSqrBraceOpt.getParent()).getType().struct;
		} else if (withSqrBraceOpt.getParent().getClass() == FormParsExtListS.class) {
			formParmName =  ((FormParsExtListS) withSqrBraceOpt.getParent()).getFormParmName();
			formParmType = ((FormParsExtListS) withSqrBraceOpt.getParent()).getType().struct;
		} else okFormParm = false;
		if (okFormParm) { 
			currentMethod.setLevel(currentMethod.getLevel()+1);
			Tab.insert(Obj.Var, formParmName, new Struct(Struct.Array, formParmType));
		}
	}
	
	public void visit(NoSqrBraceOpt noSqrBraceOpt) {
		String varName = null;
		boolean okVar = true;
		if (noSqrBraceOpt.getParent().getClass() == SVarDecl.class) 
			varName = ((SVarDecl) noSqrBraceOpt.getParent()).getVarName();
		else if (noSqrBraceOpt.getParent().getClass() == VarDeclExtListS.class)
			varName = ((VarDeclExtListS) noSqrBraceOpt.getParent()).getVarName();
		else okVar = false;
		if (okVar) {
			Obj temp;
			if (currentClass != null && currentMethod == null) temp = Tab.insert(Obj.Fld, varName, varType);
			else temp = Tab.insert(Obj.Var, varName, varType);
			if (currentClass != null) 
				temp.setFpPos(accessRight);
			report_info("Variable " + varName + " declared", noSqrBraceOpt.getParent(), temp);
		}

		String formParmName = null;
		boolean okFormParm = true;
		Struct formParmType = null;
		if (noSqrBraceOpt.getParent().getClass() == SFormPars.class) {
			formParmName =  ((SFormPars) noSqrBraceOpt.getParent()).getFormParmName();
			formParmType = ((SFormPars) noSqrBraceOpt.getParent()).getType().struct;
		} else if (noSqrBraceOpt.getParent().getClass() == FormParsExtListS.class) {
			formParmName =  ((FormParsExtListS) noSqrBraceOpt.getParent()).getFormParmName();
			formParmType = ((FormParsExtListS) noSqrBraceOpt.getParent()).getType().struct;
		} else okFormParm = false;
		if (okFormParm) {
			currentMethod.setLevel(currentMethod.getLevel()+1);
			Tab.insert(Obj.Var, formParmName, formParmType); 
		}
	}
	
	public void visit(MethodTypeName methodTypeName) {
		if (currentClass != null) overridenObj = Tab.currentScope.findSymbol(methodTypeName.getMethName());
		if (overridenObj != null) {
			currentMethod = overridenObj;
			overridenObj = new Obj(overridenObj.getKind(), overridenObj.getName(), overridenObj.getType(), overridenObj.getAdr(), overridenObj.getLevel());
			if (overridenObj.getKind() != Obj.Meth)
				report_error("Only method can be overriden! ", methodTypeName);
			if (!(overridenObj.getType()).equals(methodTypeName.getXType().struct))
				report_error("Overriden method must have same return type! ", methodTypeName);
		} else {
			currentMethod = Tab.insert(Obj.Meth, methodTypeName.getMethName(), methodTypeName.getXType().struct);
		}
		methodTypeName.obj = currentMethod;
		
		Tab.openScope();
		
		if (currentClass != null) {
			Tab.insert(Obj.Var, "this", currentClass.getType());
			currentMethod.setLevel(1);
		} else
			currentMethod.setLevel(0);
		
		if (overridenObj != null) {
			Obj thisObj = (new LinkedList<Obj>(currentMethod.getLocalSymbols())).get(0);
			if (thisObj.getKind() != Obj.Var || !thisObj.getName().equals("this"))
				report_error("Parameter this must be variable type! ", methodTypeName);
		}
		
		if (currentClass != null) {			
			SyntaxNode grandParent = ((MethodDecl) methodTypeName.getParent()).getParent();
			if (grandParent.getClass() == PrivateMethodDeclAccessListS.class || grandParent.getClass() == PrivateMethodXMethodDeclList.class)
				currentMethod.setFpPos(1);
			else if (grandParent.getClass() == ProtectedMethodDeclAccessListS.class || grandParent.getClass() == ProtectedMethodXMethodDeclList.class)
				currentMethod.setFpPos(2);
			else if (grandParent.getClass() == PublicMethodDeclAccessListS.class || grandParent.getClass() == PublicMethodXMethodDeclList.class)
				currentMethod.setFpPos(3);
		} else {
			currentMethod.setFpPos(0);
		}
	}
	
	public void visit(MethodDecl methodDecl) {
		if (!returnFound && currentMethod.getType() != Tab.noType)
			report_error("Function " + currentMethod.getName() + " doesn't have return statement! ", methodDecl);
		if (overridenObj != null) checkIfCorrectOverriden(methodDecl);
		Tab.chainLocalSymbols(currentMethod);
		Tab.closeScope();
		
		String methName = methodDecl.getMethodTypeName().getMethName();
		if (methName.equals("main")) {
			mainExists = true;
			if (currentMethod.getType() != Tab.noType) 
				report_error("Function main must return void! ", methodDecl);
			if (currentMethod.getLevel() != 0)
				report_error("Function main should not have formal parameters! ", methodDecl);
		}
		
		returnFound = false;
		currentMethod = null;
		overridenObj = null;
	}
	
	public void visit(AbstractMethodTypeName abstractMethodTypeName) {
		if (currentClass != null) overridenObj = Tab.currentScope.findSymbol(abstractMethodTypeName.getAbstMethName());
		if (overridenObj != null) {
			currentMethod = overridenObj;
			overridenObj = new Obj(overridenObj.getKind(), overridenObj.getName(), overridenObj.getType(), overridenObj.getAdr(), overridenObj.getLevel());
			if (overridenObj.getKind() != Obj.Meth)
				report_error("Only method can be overriden! ", abstractMethodTypeName);
			if (!(overridenObj.getType()).equals(abstractMethodTypeName.getXType().struct))
				report_error("Overriden method must have same return type! ", abstractMethodTypeName);
		} else {
			currentMethod = Tab.insert(Obj.Meth, abstractMethodTypeName.getAbstMethName(), abstractMethodTypeName.getXType().struct);
		}
		abstractMethodTypeName.obj = currentMethod;
		
		Tab.openScope();
		
		if (currentClass != null) {
			Tab.insert(Obj.Var, "this", currentClass.getType());
			currentMethod.setLevel(1);
		} else
			currentMethod.setLevel(0);
		
		if (overridenObj != null) {
			Obj thisObj = (new LinkedList<Obj>(currentMethod.getLocalSymbols())).get(0);
			if (thisObj.getKind() != Obj.Var || !thisObj.getName().equals("this"))
				report_error("Parameter this must be variable type! ", abstractMethodTypeName);
		}
		
		SyntaxNode grandParent = ((AbstractMethodDecl) abstractMethodTypeName.getParent()).getParent();
		if (grandParent.getClass() == PrivateAbstractXMethodDeclList.class)
			currentMethod.setFpPos(1 + 4);
		else if (grandParent.getClass() == ProtectedAbstractXMethodDeclList.class)
			currentMethod.setFpPos(2 + 4);
		else if (grandParent.getClass() == PublicAbstractXMethodDeclList.class)
			currentMethod.setFpPos(3 + 4);
		
		if (currentClass.getFpPos() != 4)
			report_error("Only abstract class can contain abstract methods! ", abstractMethodTypeName);
	}
	
	public void visit(AbstractMethodDecl abstractMethodDecl) {
		if (overridenObj != null) checkIfCorrectOverriden(abstractMethodDecl);
		Tab.chainLocalSymbols(currentMethod);
		Tab.closeScope();
		
		currentMethod = null;
		overridenObj = null;
	}
	
	public void visit(TypeXType typeXType) {
		typeXType.struct = typeXType.getType().struct;
	}
	
	public void visit(VoidXType voidXType) {
		voidXType.struct = Tab.noType;
	}
	
	public void visit(Type type) {
		Obj typeNode = Tab.find(type.getTypeName());
		if (typeNode == Tab.noObj) {
			report_error("Type " + type.getTypeName() + " doesn't exist! ", type);
			type.struct = Tab.noType;
		} else if (typeNode.getKind() != Obj.Type) {
			report_error(type.getTypeName() + " is not a type! ", type);
			type.struct = Tab.noType;
		} else {
			type.struct = typeNode.getType();
		}
	}
	
	public void visit(DesignatorEnd designatorEnd) {
		if (designatorEnd.getParent().getClass() == Designator.class) {
			Designator parent = (Designator) designatorEnd.getParent();
			
			Obj temp = Tab.find(parent.getDesName());
	    	if (temp == Tab.noObj)
				report_error(parent.getDesName() + " is not declared! ", parent);
			currentDesignatorList.add(temp);
			designatorEnd.obj = temp;
			
			checkIfPrivateOrProtected(temp, currentClass, parent);
			
			if (!(temp.getName()).equals("this") && currentClass != null && Tab.currentScope.findSymbol(temp.getName()) == null)
				parentClasses.add(Tab.find("this"));
			else 
				parentClasses.add(Tab.noObj);
			
		} else if (designatorEnd.getParent().getClass() == DotXDesignatorExtList.class) {
			DotXDesignatorExtList parent = (DotXDesignatorExtList) designatorEnd.getParent();
			Obj current = currentDesignatorList.removeLast();
			
			if (current.getType().getKind() != Struct.Class) {
				report_error("Incorrect type of " + current.getName() + "! ", parent);
				currentDesignatorList.add(Tab.noObj);
			} else {
				boolean ok = false;
				Obj hlpObj = null;
				if (currentClass != null && current.getType() == currentClass.getType()) {
					if (currentMethod == null) hlpObj = Tab.currentScope.findSymbol(parent.getDotDesName());
					else hlpObj = Tab.currentScope.getOuter().findSymbol(parent.getDotDesName());
					if (hlpObj != null) ok = true;
				} else {
					LinkedList<Obj> fields = new LinkedList<Obj>(current.getType().getMembers());
					for (Obj f: fields) {
						if ((f.getName()).equals(parent.getDotDesName())) {
							hlpObj = f;
							ok = true; 
							break;
						}
					}
				}
				if (!ok) {
					report_error(parent.getDotDesName() + " is not declared! ", parent);
					currentDesignatorList.add(Tab.noObj);
				} else {
					Obj newObj;
					if (hlpObj.getKind() == Obj.Meth) newObj = new Obj(hlpObj.getKind(), hlpObj.getName(), hlpObj.getType(), hlpObj.getAdr(), hlpObj.getLevel());
					else newObj = new Obj(Obj.Fld, hlpObj.getName(), hlpObj.getType(), hlpObj.getAdr(), hlpObj.getLevel());
					newObj.setLocals(getLocalSymbolsDataStructure(hlpObj));
					newObj.setFpPos(hlpObj.getFpPos());
					currentDesignatorList.add(newObj);
					designatorEnd.obj = newObj;
				}
				
				checkIfPrivateOrProtected(hlpObj, current, parent);
				
				if (hlpObj.getKind() == Obj.Meth) report_info("Accessing method " + hlpObj.getName() + " of class " + current.getName(), parent, hlpObj);
				else report_info("Accessing field " + hlpObj.getName() + " of class " + current.getName(), parent, hlpObj);
			}
		
		} else if (designatorEnd.getParent().getClass() == SqrBraceXDesignatorExtList.class) {
			SqrBraceXDesignatorExtList parent = (SqrBraceXDesignatorExtList) designatorEnd.getParent();
			Obj current = currentDesignatorList.removeLast();
		
			if (current.getType().getKind() != Struct.Array) {
				report_error("Incorrect type of " + current.getName() + "! ", parent);
				currentDesignatorList.add(Tab.noObj);
			} else {
				Obj newObj = new Obj(Obj.Elem, current.getName(), current.getType().getElemType(), current.getAdr(), current.getLevel());
				newObj.setFpPos(current.getFpPos());
				currentDesignatorList.add(newObj);
				designatorEnd.obj = newObj;
			}
			report_info("Accessing element of array " + current.getName(), parent.getParent(), current);
		}
	}
	
	public void visit(Designator designator) {
		designator.obj = currentDesignatorList.removeLast();
		
		designator.getDesignatorStart().obj = parentClasses.removeLast();
	}
	
	public void visit(SqrBraceXDesignatorExtList sqrBraceXDesignatorExtList) { 
		if (sqrBraceXDesignatorExtList.getExpr().obj.getType() != Tab.intType)
			report_error("Incorrect type of expression! ", sqrBraceXDesignatorExtList);
	}	
	
	public void visit(DesignFactor designFactor) {
		designFactor.obj = designFactor.getDesignator().obj; 
	}
	
	public void visit(NumConstFactor numConstFactor) {
		numConstFactor.obj = new Obj(Obj.Con, "_?", Tab.intType, numConstFactor.getN1(), Obj.NO_VALUE);
	}
	
	public void visit(CharConstFactor charConstFactor) {
		charConstFactor.obj = new Obj(Obj.Con, "_?", Tab.charType, charConstFactor.getC1(), Obj.NO_VALUE);
	}
	
	public void visit(TrueConstFactor trueConstFactor) {
		trueConstFactor.obj = new Obj(Obj.Con, "_?", boolType, 1, Obj.NO_VALUE);
	}
	
	public void visit(FalseConstFactor falseConstFactor) {
		falseConstFactor.obj = new Obj(Obj.Con, "_?", boolType, 0, Obj.NO_VALUE);
	}
	
	public void visit(ParenFactor parenFactor) {
		parenFactor.obj = parenFactor.getExpr().obj;
	}
	
	/*	sym_old; terminal SPEC_SYM; nonterminal SpecName
	"$"	{ return new_symbol(sym.SPEC_SYM, yytext());}
   (SpecFactor) SpecName Designator;
	SpecName ::= (SpecName) SPEC_SYM;
	*/
	/*
	public void visit(SpecFactor specFactor) {
		if (specFactor.getDesignator().obj.getType().getKind() != Struct.Array || (specFactor.getDesignator().obj.getKind() != Obj.Var && specFactor.getDesignator().obj.getKind() != Obj.Fld && specFactor.getDesignator().obj.getKind() != Obj.Elem))
			report_error("Designator in specific operation must be array type! ", specFactor);
		if (specFactor.getDesignator().obj.getType().getElemType() != Tab.intType)
			report_error("Specific operation only works with an array of integers! ", specFactor);
		specFactor.obj = new Obj(Obj.Con, "_?", Tab.intType, Obj.NO_VALUE, Obj.NO_VALUE);
	}
	*/
	
	public void visit(NewFactor newFactor) {
		newFactor.obj = new Obj(Obj.Var, "_?", newFactor.getFactorExprExtOpt().struct); 
	}
		
	public void visit(WithFactorExprExtOpt withFactorExprExtOpt) {
		NewFactor parent = (NewFactor) withFactorExprExtOpt.getParent();
		withFactorExprExtOpt.struct = new Struct(Struct.Array, parent.getType().struct);
		
		if (withFactorExprExtOpt.getExpr().obj.getType() != Tab.intType)
			report_error("Incorrect type of expression! ", withFactorExprExtOpt);
	}
	
	public void visit(NoFactorExprExtOpt noFactorExprExtOpt) {
		NewFactor parent = (NewFactor) noFactorExprExtOpt.getParent();
		if (parent.getType().struct.getKind() != Struct.Class)
			report_error("Incorrect type in new statement! ", noFactorExprExtOpt);	
		noFactorExprExtOpt.struct =  parent.getType().struct;

		if ((Tab.find(parent.getType().getTypeName())).getFpPos() == 4) {
			report_error("Abstract class can not be instantiated! ", noFactorExprExtOpt);
		}
	}
	
	public void visit(Term term) {
		term.obj = term.getFactor().obj;
		
		if (term.getTermExtList() instanceof TermExtListS)
			term.obj = term.getTermExtList().obj;
	}
	
	public void visit(TermExtListS termExtListS) {
		termExtListS.obj = termExtListS.getFactor().obj;
		Obj parentObj = null;
		if (termExtListS.getFactor().obj.getType() != Tab.intType)
			report_error("Incorrect type of factor in term! ", termExtListS);	
		if (termExtListS.getParent().getClass() == Term.class) {
			Term parent = (Term) termExtListS.getParent();
			parentObj = parent.getFactor().obj;
			if (parent.getFactor().obj.getType() != Tab.intType)
				report_error("Incorrect type of factor in term! ", termExtListS);	
		} else if (termExtListS.getParent().getClass() == TermExtListS.class) {
			TermExtListS parent = (TermExtListS) termExtListS.getParent();
			parentObj = parent.getFactor().obj;
		}
		
		if (termExtListS.getMulop() instanceof MulopMulopRight && parentObj.getKind() != Obj.Var && parentObj.getKind() != Obj.Elem && parentObj.getKind() != Obj.Fld)
			report_error("Incorrect use of assignment operator! ", termExtListS);
		
		if (termExtListS.getTermExtList() instanceof TermExtListS)
			termExtListS.obj = termExtListS.getTermExtList().obj;
	}

	public void visit(Expr expr) {
		expr.obj = expr.getSubOpt().obj;
	}
	
	public void visit(WithSubOpt withSubOpt) {
		withSubOpt.obj = withSubOpt.getTerm().obj;
		if (withSubOpt.getTerm().obj.getType() != Tab.intType)
			report_error("Incorrect type of expression! ", withSubOpt);	
	}
	
	public void visit(NoSubOpt noSubOpt) {
		noSubOpt.obj = noSubOpt.getTerm().obj;
	}
	
	public void visit(ExprExtListS exprExtListS) {
		exprExtListS.obj = exprExtListS.getTerm().obj;
		Obj parentObj = null;
		if (exprExtListS.getTerm().obj.getType() != Tab.intType)
			report_error("Incorrect type of expression! ", exprExtListS);	
		if (exprExtListS.getParent().getClass() == Expr.class) {
			Expr parent = (Expr) exprExtListS.getParent();
			parentObj = parent.getSubOpt().obj;
			if (parent.getSubOpt().obj.getType() != Tab.intType)
				report_error("Incorrect type of expression! ", exprExtListS);	
		} else if (exprExtListS.getParent().getClass() == ExprExtListS.class) {
			ExprExtListS parent = (ExprExtListS) exprExtListS.getParent();
			parentObj = parent.getTerm().obj;
		}
		
		if (exprExtListS.getAddop() instanceof AddopAddopRight && parentObj.getKind() != Obj.Var && parentObj.getKind() != Obj.Elem && parentObj.getKind() != Obj.Fld)
			report_error("Incorrect use of assignment operator! ", exprExtListS);
	}
	
	public void visit(WithCondFactExtOpt withCondFactExtOpt) {
		Struct parentStruct = ((CondFact) withCondFactExtOpt.getParent()).getExpr().obj.getType();
		Struct withCondFactStruct = withCondFactExtOpt.getExpr().obj.getType();
		if (!withCondFactStruct.compatibleWith(parentStruct))
			report_error("Incompatible types in condition! ", withCondFactExtOpt);

		if (parentStruct.getKind() == Struct.Array || parentStruct.getKind() == Struct.Class || withCondFactStruct.getKind() == Struct.Array || withCondFactStruct.getKind() == Struct.Class) {
			if (withCondFactExtOpt.getRelop() instanceof GtRelop) {
				report_error("Incorrect type in condition with >! ", withCondFactExtOpt.getParent());	
			} else if (withCondFactExtOpt.getRelop() instanceof GeRelop) {
				report_error("Incorrect type in condition with >=! ", withCondFactExtOpt.getParent());
			} else if (withCondFactExtOpt.getRelop() instanceof LtRelop) {
				report_error("Incorrect type in condition with <! ", withCondFactExtOpt.getParent());
			} else if (withCondFactExtOpt.getRelop() instanceof LeRelop) {
				report_error("Incorrect type in condition with <=! ", withCondFactExtOpt.getParent());
			}
		}
	}
	
	public void visit(NoCondFactExtOpt noCondFactExtOpt) {
		CondFact parent = (CondFact) noCondFactExtOpt.getParent();
		if (parent.getExpr().obj.getType() != boolType)
			report_error("Expression in condition must be bool type! ", noCondFactExtOpt);	
	}
	
	public void visit(Condition condition) {
		condition.struct = boolType;
	}
	
	public void visit(WithConditionOpt withConditionOpt) {
		withConditionOpt.struct = withConditionOpt.getCondition().struct;
	}
	
	public void visit(ErrorConditionOpt errorConditionOpt) {
		errorConditionOpt.struct = Tab.noType;
	}
	
	public void visit(NoConditionOpt noConditionOpt) {
		noConditionOpt.struct = boolType;
	}
	
	public void visit(WithFactorActParsOpt withFactorActParsOpt) {
		DesignFactor parent = (DesignFactor) withFactorActParsOpt.getParent();
		Obj func = parent.getDesignator().obj;
		
		report_info("Detected call of function " + func.getName(), parent, func);
		
		if (func.getKind() != Obj.Meth)
			report_error(func.getName() + " is not a function! ", parent.getDesignator());
		
		if (func.getType() == Tab.noType)
			report_error(func.getName() + " can not be used in expression! ", parent.getDesignator());
		
		if (!checkActPars())
			report_error("Mismatch of formal and actual parameters in function " + func.getName() + "! ", parent.getDesignator());
		
		currentCalls.removeLast();
		actParsPerMeth.removeLast();
	}
	
	public void visit(ActParsXDesignatorStatementExt actParsXDesignatorStatementExt) {
		DesignatorStatement parent = (DesignatorStatement) actParsXDesignatorStatementExt.getParent();
		Obj func = parent.getDesignator().obj;
		
		report_info("Detected call of function " + func.getName(), parent, func);

		if (func.getKind() != Obj.Meth)
			report_error(func.getName() + " is not a function! ", parent.getDesignator());

		if (!checkActPars())
			report_error("Mismatch of formal and actual parameters in function " + func.getName() + "! ", parent.getDesignator());
		
		currentCalls.removeLast();
		actParsPerMeth.removeLast();
	}
	
	public void visit(ActParsStart actParsStart) {
		WithActParsOpt parent = (WithActParsOpt) actParsStart.getParent();
		if (parent.getParent().getClass() == ActParsXDesignatorStatementExt.class) {
			ActParsXDesignatorStatementExt grandParent = (ActParsXDesignatorStatementExt) parent.getParent();
			DesignatorStatement grandGrandParent = (DesignatorStatement) grandParent.getParent();
			currentCalls.add(grandGrandParent.getDesignator().obj);
		} else if (parent.getParent().getClass() == WithFactorActParsOpt.class) {
			WithFactorActParsOpt grandParent = (WithFactorActParsOpt) parent.getParent();
			DesignFactor grandGrandParent = (DesignFactor) grandParent.getParent();
			currentCalls.add(grandGrandParent.getDesignator().obj);
		}
		actParsPerMeth.add(new LinkedList<Struct>());
	}
	
	public void visit(NoActParsOpt noActParsOpt) {
		if (noActParsOpt.getParent().getClass() == ActParsXDesignatorStatementExt.class) {
			ActParsXDesignatorStatementExt parent = (ActParsXDesignatorStatementExt) noActParsOpt.getParent();
			DesignatorStatement grandParent = (DesignatorStatement) parent.getParent();
			currentCalls.add(grandParent.getDesignator().obj);
		} else if (noActParsOpt.getParent().getClass() == WithFactorActParsOpt.class) {
			WithFactorActParsOpt parent = (WithFactorActParsOpt) noActParsOpt.getParent();
			DesignFactor grandParent = (DesignFactor) parent.getParent();
			currentCalls.add(grandParent.getDesignator().obj);
		}
		actParsPerMeth.add(new LinkedList<Struct>());
	}
	
	public void visit(ActParsEnd actParsEnd) {
		Struct actParStruct = null;
		if (actParsEnd.getParent().getClass() == ActPars.class)
			actParStruct = ((ActPars) actParsEnd.getParent()).getExpr().obj.getType();
		else if (actParsEnd.getParent().getClass() == ActParsExtListS.class)
			actParStruct = ((ActParsExtListS) actParsEnd.getParent()).getExpr().obj.getType();
		
		actParsPerMeth.getLast().add(actParStruct);
	}
	
	public void visit(AssignopXDesignatorStatementExt assignopXDesignatorStatementExt) {
		DesignatorStatement parent = (DesignatorStatement) assignopXDesignatorStatementExt.getParent();
		if (parent.getDesignator().obj.getKind() != Obj.Var && parent.getDesignator().obj.getKind() != Obj.Elem && parent.getDesignator().obj.getKind() != Obj.Fld)
			report_error("Incompatible types in assignment! ", assignopXDesignatorStatementExt);
		
		if (!checkIfReferenceOnExtendedClass(TypeOfCheck.ASSIGNABLE_TO, assignopXDesignatorStatementExt.getExpr().obj.getType(), parent.getDesignator().obj.getType()))
			report_error("Incompatible types in assignment! ", assignopXDesignatorStatementExt);
		
		if (assignopXDesignatorStatementExt.getAssignop() instanceof AddopRightAssignop || assignopXDesignatorStatementExt.getAssignop() instanceof MulopRightAssignop)
			if (assignopXDesignatorStatementExt.getExpr().obj.getType() != Tab.intType || parent.getDesignator().obj.getType() != Tab.intType)
				report_error("Incompatible types in assignment (must be int)! ", assignopXDesignatorStatementExt);
	}
	
	public void visit(IncrXDesignatorStatementExt incrXDesignatorStatementExt) {
		DesignatorStatement parent = (DesignatorStatement) incrXDesignatorStatementExt.getParent();
		if (parent.getDesignator().obj.getKind() != Obj.Var && parent.getDesignator().obj.getKind() != Obj.Elem && parent.getDesignator().obj.getKind() != Obj.Fld)
			report_error("Incompatible types in incrementing! ", incrXDesignatorStatementExt);
		if (parent.getDesignator().obj.getType() != Tab.intType)
			report_error("Incompatible types in incrementing! ", incrXDesignatorStatementExt);	
	}
	
	public void visit(DecrXDesignatorStatementExt decrXDesignatorStatementExt) {
		DesignatorStatement parent = (DesignatorStatement) decrXDesignatorStatementExt.getParent();
		if (parent.getDesignator().obj.getKind() != Obj.Var && parent.getDesignator().obj.getKind() != Obj.Elem && parent.getDesignator().obj.getKind() != Obj.Fld)
			report_error("Incompatible types in decrementing! ", decrXDesignatorStatementExt);
		if (parent.getDesignator().obj.getType() != Tab.intType)
			report_error("Incompatible types in decrementing! ", decrXDesignatorStatementExt);	
	}
	
	public void visit(BreakStatement breakStatement) {
		if (forLevelCounter==0)
			report_error("Break statement can be used only inside for loop! ", breakStatement);
	}
	
	public void visit(ContinueStatement continueStatement) {
		if (forLevelCounter==0)
			report_error("Continue statement can be used only inside for loop! ", continueStatement);
	}
	
	public void visit(ReadStatement readStatement) {
		if (readStatement.getDesignator().obj.getKind() != Obj.Var && readStatement.getDesignator().obj.getKind() != Obj.Elem && readStatement.getDesignator().obj.getKind() != Obj.Fld)
			report_error("Incompatible types in read statement! ", readStatement);
		if (readStatement.getDesignator().obj.getType() != Tab.intType && 
				readStatement.getDesignator().obj.getType() != Tab.charType && 
				readStatement.getDesignator().obj.getType() != boolType)
			report_error("Incompatible types in read statement! ", readStatement);	
	}
	
	public void visit (PrintStatement printStatement) {
		if (printStatement.getExpr().obj.getType() != Tab.intType && 
				printStatement.getExpr().obj.getType() != Tab.charType && 
				printStatement.getExpr().obj.getType() != boolType)
			report_error("Incompatible types in print statement! ", printStatement);	
	}

	public void visit(WithExprOpt withExprOpt) {
		returnFound = true;
    	Struct currMethType = currentMethod.getType();
    	if (!currMethType.equals(withExprOpt.getExpr().obj.getType()))
    		report_error("Incompatible types in return statement! ", withExprOpt);
	}
	
	public void visit(ForName forName) {
		report_info("For loop detected", forName.getParent(), null);
		forLevelCounter++;
	}
	
	public void visit(ForeachName foreachName) {
		report_info("Foreach loop detected", foreachName.getParent(), null);
		forLevelCounter++;
	}
	
	public void visit(ForStatement forStatement) {
		forLevelCounter--;
		if (forStatement.getConditionOpt().struct != boolType)
			report_error("Condition in for loop must be bool type! ", forStatement);
	}
	
	public void visit(ForeachStatement foreachStatement) {
		forLevelCounter--;
		if (foreachStatement.getDesignator().obj.getType().getKind() != Struct.Array || ((foreachStatement.getDesignator().obj.getKind() != Obj.Var && foreachStatement.getDesignator().obj.getKind() != Obj.Fld))) //
			report_error("Designator in foreach loop must be array type! ", foreachStatement);
		Obj temp = Tab.find(foreachStatement.getI2());
    	if (temp == Tab.noObj)
			report_error("Variable" + foreachStatement.getI2() + " doesn't exist! ", foreachStatement);
    	if (temp.getKind() != Obj.Var)
    		report_error(foreachStatement.getI2() + " is not a variable! ", foreachStatement);
    	if (!(temp.getType()).equals(foreachStatement.getDesignator().obj.getType().getElemType()))
    		report_error("Variable " + foreachStatement.getI2() + " and element of " + foreachStatement.getDesignator().obj.getName() + " doesn't have equivalent types! ", foreachStatement);
		
    	foreachStatement.getForeachName().obj = temp;
	}
	
	public void visit(IfElseStatement ifElseStatement) {
		if (ifElseStatement.getCondition().struct != boolType)
			report_error("Condition in if-else statement must be bool type! ", ifElseStatement);
	}
	
	public void visit(IfStatement ifStatement) {
		if (ifStatement.getCondition().struct != boolType)
			report_error("Condition in if statement must be bool type! ", ifStatement);
	}
	
	public void visit(ClassName className) {
		currentClass = Tab.insert(Obj.Type, className.getClassName(), new Struct(Struct.Class));
		currentClass.setFpPos(0);
		Tab.openScope();
	}
	
	public void visit(AbstractClassName abstractClassName) {
		currentClass = Tab.insert(Obj.Type, abstractClassName.getAbstClassName(), new Struct(Struct.Class));
		currentClass.setFpPos(4); //naznaka da je apstraktna klasa
		Tab.openScope();
	}
	
	public void visit(NoExtendsOpt noExtendsOpt) {
		Tab.insert(Obj.Fld, "#vftp", Tab.intType);
	}
	
	public void visit(WithExtendsOpt withExtendsOpt) {
		if (withExtendsOpt.getType().struct.getKind() != Struct.Class) 
			report_error(withExtendsOpt.getType().getTypeName() + " is not a class type! ", withExtendsOpt);
				
		currentClass.getType().setElementType(withExtendsOpt.getType().struct);

		LinkedList<Obj> fields = new LinkedList<Obj>(withExtendsOpt.getType().struct.getMembers());
		for (Obj f: fields) {
			Obj hlpF = Tab.insert(f.getKind(), f.getName(), f.getType());
			//hlpF.setAdr(f.getAdr());
			hlpF.setFpPos(f.getFpPos());
			if (f.getKind() == Obj.Meth) {
				hlpF.setLevel(f.getLevel());
				LinkedList<Obj> locals = new LinkedList<Obj>(f.getLocalSymbols());
				Tab.openScope();
				for (Obj l: locals) {
					Obj hlpL = Tab.insert(l.getKind(), l.getName(), l.getType());
					//hlpL.setAdr(l.getAdr());
					hlpL.setFpPos(l.getFpPos());
				}				
				Tab.chainLocalSymbols(hlpF);
				Tab.closeScope();
			}
		}
	}
	
	public void visit(SClassDecl sClassDecl) {
		Tab.chainLocalSymbols(currentClass.getType());
		Tab.closeScope();
		
		LinkedList<Obj> objNodes = new LinkedList<Obj>(currentClass.getType().getMembers());
		for (Obj o: objNodes) {
			if (o.getFpPos() > 4)
				report_error("Non-abstract class must implement abstract methods! ", sClassDecl);
		}
		
		sClassDecl.obj = currentClass;
		currentClass = null;
	}
	
	public void visit(AbstractClassDecl abstractClassDecl) {
		Tab.chainLocalSymbols(currentClass.getType());
		Tab.closeScope();
		
		report_info("Abstract class " + currentClass.getName() + " declared ", abstractClassDecl, currentClass);
		abstractClassDecl.obj = currentClass;
		currentClass = null;
	}
	
	private boolean checkActPars() {
		LinkedList<Struct> actPars = actParsPerMeth.getLast();
		int numOfFormPars = currentCalls.getLast().getLevel();
		
		int haveThis = 0;
		if (currentCalls.getLast().getFpPos() > 0) {
			haveThis = 1;
			numOfFormPars--;
		}
		if (numOfFormPars != actPars.size()) 
			return false;
		
		boolean ok = true;
		LinkedList<Obj> formParsAndLocals = new LinkedList<Obj>(currentCalls.getLast().getLocalSymbols());			
		for (int i = 0; i < numOfFormPars; i++)
			if (!checkIfReferenceOnExtendedClass(TypeOfCheck.ASSIGNABLE_TO, actPars.get(i), formParsAndLocals.get(i+haveThis).getType())) {
				ok = false;
				break;
			}
		return ok;
	}
	
	private void checkIfCorrectOverriden(SyntaxNode info) {
		if (overridenObj.getLevel() != currentMethod.getLevel())
			report_error("Overriden method must have same nuber of parameters! ", info);
			
		LinkedList<Obj> temp1 = new LinkedList<Obj>(currentMethod.getLocalSymbols());
		LinkedList<Obj> temp2 = new LinkedList<Obj>(Tab.currentScope.values());
		
		for (int i = 1; i < overridenObj.getLevel(); i++) {
			if (!(temp1.get(i).getType()).equals(temp2.get(i).getType()))
				report_error("Overriden method must have parameters with same type! ", info);
		}			
	}
	
	private SymbolDataStructure getLocalSymbolsDataStructure(Obj node) {
		SymbolDataStructure locals = SymbolTableFactory.instance().createSymbolTableDataStructure();
		
		LinkedList<Obj> temp = new LinkedList<Obj>(node.getLocalSymbols());
		for (Obj o: temp) {
			locals.insertKey(o); //
		}
		return locals;
	}
	
	private void checkIfPrivateOrProtected(Obj obj, Obj parentClassObj, SyntaxNode node) {
		if (obj == null || obj == Tab.noObj) return;
		
		if (obj.getFpPos() == 1) {
			if (currentClass == null || currentClass.getType() != parentClassObj.getType()) //.getName()
				report_error("Can not access private field " + obj.getName() + "! ", node);
			else if (currentClass.getType().getElemType() != null) {
				LinkedList<Obj> objNodes = new LinkedList<Obj>(currentClass.getType().getElemType().getMembers());
				for (Obj o: objNodes) {
					if (o.getName().equals(obj.getName())) {
						report_error("Can not access private field " + o.getName() + "! ", node);
						break;
					}
				}
			}
			
		} else if (obj.getFpPos() == 2) {
			if (currentClass == null)
				report_error("Can not access protected field " + obj.getName() + "! ", node);
			else {
				Struct tempStruct = currentClass.getType();
				while (parentClassObj.getType() != tempStruct) {
					tempStruct = tempStruct.getElemType();
					if (tempStruct == null) {
						report_error("Can not access protected field " + obj.getName() + "! ", node);
						break;
					}
				}
			}
			
		}
	}
	
	private boolean checkIfReferenceOnExtendedClass(TypeOfCheck type, Struct dest, Struct src) {	
		Struct temp = dest;
		while (!checkOperation(type, temp, src) && temp.getKind() == Struct.Class) { 
			if (temp.getElemType() != null)
				temp = temp.getElemType();
			else break;
		}
		if (!checkOperation(type, temp, src))
			return false;
		else return true;
	}
	
	private boolean checkOperation(TypeOfCheck type, Struct dest, Struct src) {
		boolean ok;
		switch (type) {
		case EQUALS:
			ok = dest.equals(src);
			break;
		case COMPATIBLE_WITH:
			ok = dest.compatibleWith(src);
			break;
		case ASSIGNABLE_TO:
			ok = dest.assignableTo(src);
			break;
		default:
			ok = false;
			break;
		}
		return ok;
	}
	
	public boolean passed() {
		return !errorDetected;
	}
	
	public void tsdump() {
		//Tab.dump();

		log.info("=====================SYMBOL TABLE DUMP=========================");
		MyDumpSymbolTableVisitor stv = new MyDumpSymbolTableVisitor();
		for (Scope s = Tab.currentScope; s != null; s = s.getOuter()) {
			s.accept(stv);
		}
		log.info(stv.getOutput());
	}
}
