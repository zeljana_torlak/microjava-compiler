package rs.ac.bg.etf.pp1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rs.etf.pp1.mj.runtime.Code;

public class VirtualMethodTable {
	
	private List<Byte> MethodTable = new ArrayList<Byte>();
	
	private LinkedList<String> classList = new LinkedList<String>();
	private LinkedList<Integer> vftpAddrList = new LinkedList<Integer>();
	
	private int size = 0;
	
	private void addWordToStaticData (int value, int address) {
		MethodTable.add(new Byte((byte) Code.const_));
		MethodTable.add(new Byte((byte) ((value >> 16) >> 8)));
		MethodTable.add(new Byte((byte) (value >> 16)));
		MethodTable.add(new Byte((byte) (value >> 8)));
		MethodTable.add(new Byte((byte) value));
		MethodTable.add(new Byte((byte) Code.putstatic));
		MethodTable.add(new Byte((byte) (address >> 8)));
		MethodTable.add(new Byte((byte) address));
		
		size++;
	}
	
	private void addNameTerminator() {
		addWordToStaticData(-1, Code.dataSize++);
	}
	
	public void addTableTerminator() {
		addWordToStaticData(-2, Code.dataSize++);
	}
	
	private void addFunctionAddress(int functionAddress) {
		addWordToStaticData(functionAddress, Code.dataSize++);
	}
	
	public void addFunctionEntry(String name, int functionAddressInCodeBuffer) {
		for (int j = 0; j < name.length(); j++) {
			addWordToStaticData((int) (name.charAt(j)), Code.dataSize++);
		}
		addNameTerminator();
		addFunctionAddress(functionAddressInCodeBuffer);
	}
	
	public void insertTable() {
		Object ia[] = MethodTable.toArray();
		for (int i = 0; i < ia.length; i++)
			Code.buf[Code.pc++] = ((Byte) ia[i]).byteValue();
		MethodTable.clear();
	}
	
	public void classRegistration(String className) {
		classList.add(className);
		vftpAddrList.add(Code.dataSize);
	}
	
	public int getVftpAddrForClass(String className) {
		for (int i = 0; i < classList.size(); i++) {
			if (className.equals(classList.get(i))) {
				return vftpAddrList.get(i);
			}
		}
		return -1;
	}
	
	public int getSize() {
		return size;
	}
}
