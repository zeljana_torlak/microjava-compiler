package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;

public class CounterVisitor extends VisitorAdaptor {

	protected int count = 0;
	
	public int getCount() {
		return count;
	}
	
	public static class FormParCounter extends CounterVisitor {
		
		public void visit(SFormPars sFormPars) {
			count++;
		}
		
		public void visit(FormParsExtListS formParsExtListS) {
			count++;
		}
	}
	
	public static class VarCounter extends CounterVisitor {
		
		public void visit(SVarDecl sVarDecl) {
			count++;
		}
		
		public void visit(VarDeclExtListS varDeclExtListS) {
			count++;
		}
	}
}
