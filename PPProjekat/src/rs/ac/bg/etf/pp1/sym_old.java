package rs.ac.bg.etf.pp1;

public class sym_old {	
	
	// Keywords
	public static final int PROGRAM = 1;
	public static final int BREAK = 2;
	public static final int CLASS = 3;
	public static final int ABSTRACT = 4;
	public static final int ELSE = 5;
	public static final int CONST = 6;
	public static final int IF = 7;
	public static final int NEW = 8;
	public static final int PRINT = 9;
	public static final int READ = 10;
	public static final int RETURN = 11;
	public static final int VOID = 12;
	public static final int FOR = 13;
	public static final int EXTENDS = 14;
	public static final int CONTINUE = 15;
	public static final int FOREACH = 16;
	public static final int PUBLIC = 17;
	public static final int PROTECTED = 18;
	public static final int PRIVATE = 19;
	
	// Operators
	public static final int ADD = 20; //+
	public static final int SUB = 21; //-
	public static final int MUL = 22; //*
	public static final int DIV = 23; ///
	public static final int MOD = 24; //%
	public static final int EQ = 25; //==
	public static final int NEQ = 26; //!=
	public static final int GT = 27; //>
	public static final int GE = 28; //>=
	public static final int LT = 29; //<
	public static final int LE = 30; //<=
	public static final int AND = 31; //&&
	public static final int OR = 32; //||
	public static final int ASSIGN = 33; //=
	public static final int INCR = 34; //++
	public static final int DECR = 35; //--
	public static final int ADDASSIGN = 36; //+=
	public static final int SUBASSIGN = 37; //-=
	public static final int MULASSIGN = 38; //*=
	public static final int DIVASSIGN = 39; ///=
	public static final int MODASSIGN = 40; //%=
	public static final int SEMI = 41; //;
	public static final int COMMA = 42; //,
	public static final int COL = 43; //:
	public static final int DOT = 44; //.
	public static final int LPAREN = 45; //(
	public static final int RPAREN = 46; //)
	public static final int LSQRBRACE = 47; //[
	public static final int RSQRBRACE = 48; //]
	public static final int LCURBRACE = 49; //{
	public static final int RCURBRACE = 50; //}
	
	// Identifiers
	public static final int IDENT = 51;
	
	// Constants
	public static final int NUMCONST = 52;
	public static final int CHARCONST = 53;
	public static final int TRUECONST = 54;
	public static final int FALSECONST = 55;
	
	public static final int EOF = 56;
	
	//public static final int SPEC_SYM = 57;
	
}
