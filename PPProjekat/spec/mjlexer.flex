package rs.ac.bg.etf.pp1;
import java_cup.runtime.Symbol;

%%

%{

	private Symbol new_symbol(int type){
		return new Symbol(type, yyline+1, yycolumn);
	}
	
	private Symbol new_symbol(int type, Object value){
		return new Symbol(type, yyline+1, yycolumn, value);
	}

%}

%cup
%line
%column

%xstate COMMENT

%eofval{
	return new_symbol(sym.EOF);
%eofval}

%%

" "		{ }
"\b"	{ }
"\t"	{ }
"\r\n"	{ }
"\f"	{ }

"program"	{ return new_symbol(sym.PROGRAM, yytext());}
"break"		{ return new_symbol(sym.BREAK, yytext());}
"class"		{ return new_symbol(sym.CLASS, yytext());}
"abstract"	{ return new_symbol(sym.ABSTRACT, yytext());}
"else"		{ return new_symbol(sym.ELSE, yytext());}
"const"		{ return new_symbol(sym.CONST, yytext());}
"if"		{ return new_symbol(sym.IF, yytext());}
"new"		{ return new_symbol(sym.NEW, yytext());}
"print"		{ return new_symbol(sym.PRINT, yytext());}
"read"		{ return new_symbol(sym.READ, yytext());}
"return"	{ return new_symbol(sym.RETURN, yytext());}
"void"		{ return new_symbol(sym.VOID, yytext());}
"for"		{ return new_symbol(sym.FOR, yytext());}
"extends"	{ return new_symbol(sym.EXTENDS, yytext());}
"continue"	{ return new_symbol(sym.CONTINUE, yytext());}
"foreach"	{ return new_symbol(sym.FOREACH, yytext());}
"public"	{ return new_symbol(sym.PUBLIC, yytext());}
"protected"	{ return new_symbol(sym.PROTECTED, yytext());}
"private"	{ return new_symbol(sym.PRIVATE, yytext());}

"="			{ return new_symbol(sym.ASSIGN, yytext());}
"+"			{ return new_symbol(sym.ADD, yytext());}
"-"			{ return new_symbol(sym.SUB, yytext());}
"*"			{ return new_symbol(sym.MUL, yytext());}
"/"			{ return new_symbol(sym.DIV, yytext());}
"%"			{ return new_symbol(sym.MOD, yytext());}
"=="		{ return new_symbol(sym.EQ, yytext());}
"!="		{ return new_symbol(sym.NEQ, yytext());}
">"			{ return new_symbol(sym.GT, yytext());}
">="		{ return new_symbol(sym.GE, yytext());}
"<"			{ return new_symbol(sym.LT, yytext());}
"<="		{ return new_symbol(sym.LE, yytext());}
"&&"		{ return new_symbol(sym.AND, yytext());}
"||"		{ return new_symbol(sym.OR, yytext());}
"++"		{ return new_symbol(sym.INCR, yytext());}
"--"		{ return new_symbol(sym.DECR, yytext());}
"+="		{ return new_symbol(sym.ADDASSIGN, yytext());}
"-="		{ return new_symbol(sym.SUBASSIGN, yytext());}
"*="		{ return new_symbol(sym.MULASSIGN, yytext());}
"/="		{ return new_symbol(sym.DIVASSIGN, yytext());}
"%="		{ return new_symbol(sym.MODASSIGN, yytext());}
";"			{ return new_symbol(sym.SEMI, yytext());}
","			{ return new_symbol(sym.COMMA, yytext());}
":"			{ return new_symbol(sym.COL, yytext());}
"."			{ return new_symbol(sym.DOT, yytext());}
"("			{ return new_symbol(sym.LPAREN, yytext());}
")"			{ return new_symbol(sym.RPAREN, yytext());}
"["			{ return new_symbol(sym.LSQRBRACE, yytext());}
"]"			{ return new_symbol(sym.RSQRBRACE, yytext());}
"{"			{ return new_symbol(sym.LCURBRACE, yytext());}
"}"			{ return new_symbol(sym.RCURBRACE, yytext());}

"//"		{ yybegin(COMMENT);}
<COMMENT> .	{ yybegin(COMMENT);}
<COMMENT>	"\r\n" { yybegin(YYINITIAL);}

"true"		{ return new_symbol(sym.TRUECONST, yytext());}
"false"		{ return new_symbol(sym.FALSECONST, yytext());}
[0-9]+	{ return new_symbol(sym.NUMCONST, new Integer(yytext()));}
([a-z]|[A-Z])[a-z|A-Z|0-9|_]*	{ return new_symbol(sym.IDENT, yytext());}
\'[\x20-\x7E]\' { return new_symbol(sym.CHARCONST, new Character(yytext().charAt(1)));}

.	{ System.err.println("Lexer error: Line " + (yyline+1) + ", column " + yycolumn + ": " + yytext() + " ");}
