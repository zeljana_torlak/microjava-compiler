package rs.ac.bg.etf.pp1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import java_cup.runtime.Symbol;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import rs.ac.bg.etf.pp1.ast.Program;
import rs.ac.bg.etf.pp1.util.Log4JUtils;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;

public class Compiler {

	static {
		DOMConfigurator.configure(Log4JUtils.instance().findLoggerConfigFile());
		Log4JUtils.instance().prepareLogFile(Logger.getRootLogger());
	}
	
	public static void main(String[] args) throws Exception {
		
		Logger log = Logger.getLogger(Compiler.class);	
		Reader br = null;
		
		try {
			if (args.length < 1) log.error("Missing input file name! ");
			File inputFile = new File(args[0]);
			log.info("Compiling source file: " + inputFile.getAbsolutePath());
			br = new BufferedReader(new FileReader(inputFile));
			
			log.info("=====================LEXICAL ANALYSIS==========================");
			Yylex lexer = new Yylex(br);
			
			/*
			Symbol currToken = null;
			while ((currToken = lexer.next_token()).sym != sym_old.EOF) {
				if (currToken != null && currToken.value != null)
					log.info(currToken.toString() + " " + currToken.value.toString());
			}
			 */
			
			MJParser parser = new MJParser(lexer);
	        Symbol symbol = parser.parse();
	        
	        log.info("=====================SYNTAX ANALYSIS===========================");
	        Program program = (Program) (symbol.value); 
			log.info(program.toString("")); // ispis sintaksnog stabla
			
			log.info("=====================SEMANTIC ANALYSIS=========================");
			Tab.init();
			SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer();
			program.traverseBottomUp(semanticAnalyzer); 
			
			semanticAnalyzer.tsdump();
			
			if (parser.errorDetected || !semanticAnalyzer.passed()) 
				log.error("PARSING not SUCCESSFUL!");
			else {
				if (args.length < 2) log.error("Missing output file name! ");
				File outputFile = new File(args[1]);
				if(outputFile.exists()) outputFile.delete();
				
				CodeGenerator codeGenerator = new CodeGenerator();
				program.traverseBottomUp(codeGenerator);
				Code.dataSize += semanticAnalyzer.nVars;
				Code.mainPc = codeGenerator.getMainPc();
				Code.write(new FileOutputStream(outputFile));
				log.info("PARSING SUCCESSFUL!");
			} 
			
		} 
		finally {
			if (br != null) try { br.close(); } catch (IOException e1) { log.error(e1.getMessage(), e1); }
		}
	}
	
}
